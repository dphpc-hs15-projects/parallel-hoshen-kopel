#include <vector>
#include <random>
#include <cassert>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdexcept>
#include <cstdio>
#include "tclap/CmdLine.h"
#include "benchmark_macro.hpp"
#include "latticeview.h"
#include "datastructures/matrix.hpp"

#include "types.hpp"

// serial version
#include "hoshen_naive.hpp"
#include "hoshen_uf.hpp"

//#include "hoshen_openmp.hpp"
#include "hoshen_pthreads.hpp"

#include "utilities.hpp"
#include "datastructures/statistic.hpp"

#include "hoshen_uf_pthreads.hpp"

typedef std::tuple<double, std::string, std::string> benchmark_result_tuple_t;

typedef std::vector<benchmark_result_tuple_t> benchmark_result_t;

void save_benchmark_results(std::string filename, benchmark_result_t& benchmark_results) {
	std::ofstream file;
	file.open(filename.c_str());
	for (auto& row : benchmark_results) {
		static_assert(std::tuple_size<benchmark_result_tuple_t>::value == std::size_t(3),
			"benchmark_result_tuple_t has the wrong number of elements");
		
		file << std::get<0>(row) << ' ';
		file << '"' << std::get<1>(row) << "\" ";
		file << '"' << std::get<2>(row) << "\"";
		file << '\n';
	}
	file.close();
}

int main(int argc, char* argv[]){
	//parse cmd args
	TCLAP::CmdLine cmd("Hoshen kopel", ' ', "0.1 alpha");
	TCLAP::ValueArg<std::string> outputArg("o","output","Output file",false,std::string("output.") + LATTICE_DEFAULT_OUTPUT_EXT,"string", cmd);
	TCLAP::ValueArg<size_t> rowArg("m","row","number of rows",false,100,"integral type", cmd);
	TCLAP::ValueArg<size_t> colArg("n","col","number of columns",false,100,"integral type", cmd);
	TCLAP::ValueArg<calc_t> probabilityArg("p","prop","occupation probability",false,0.59,"floating point", cmd);
	TCLAP::ValueArg<std::string> benchmarkOutputArg("b","benchmark-output","Benchmark output file",false,std::string("benchmark_results.dat"),"string", cmd);
	cmd.parse( argc, argv );

	//initialize
	const size_t m = rowArg.getValue(); //number of rows
	const size_t n = colArg.getValue(); //number of columns
	const calc_t p = probabilityArg.getValue(); // 

	benchmark_result_t benchmark_results;

	Matrix<bool> field(m,n);
	Matrix<store_t> output_field(m,n);

	create_perc(field, p); // create random cluster

	//
	// Run single core benchmarks
	//
	// hoshen_kopel(field, output_field)
	/*
	hoshen_kopel_uf(field, output_field); // run caches warm
	benchmark_results.push_back(BENCHMARK(hoshen_naive(field, output_field))(10)); //run algorithm
	benchmark_results.push_back(BENCHMARK(hoshen_kopel_uf(field, output_field))(10)); //run algorithm

	// hoshen_kopel(output_field, output_field)
	create_perc(output_field, p); // create random cluster
	benchmark_results.push_back(BENCHMARK(hoshen_naive(output_field, output_field))(10)); //run algorithm
	create_perc(output_field, p); // create random cluster
	benchmark_results.push_back(BENCHMARK(hoshen_kopel_uf(output_field, output_field))(10)); //run algorithm
*/
	// phreads
	/*output_field.data.assign(output_field.data.size(), 0);
	for(size_t j = 1; j <=  std::max(1u, std::thread::hardware_concurrency()); ++j) {
		//decltype(output_field) output_fieldWork = output_field;
	
		benchmark_results.push_back(
			BENCHMARK(hoshen_kopel_pthreads(field, output_field, j))(10, std::to_string(j) + std::string(" threads"))
		);
		//std::cout << output_fieldWork(0, 0) << std::endl; //prevent optimization
	}*/
	// phreads uf
	/*
	output_field.data.assign(output_field.data.size(), 0);
	for(size_t j = 1; j <=  std::max(1u, std::thread::hardware_concurrency()); ++j) {
		//decltype(output_field) output_fieldWork = output_field;
	
		benchmark_results.push_back(
			BENCHMARK(hoshen_kopel_uf_pthreads(field, output_field, j))(10, std::to_string(j) + std::string(" threads"))
		);
		//std::cout << output_fieldWork(0, 0) << std::endl; //prevent optimization
	}
*/


	/**
	 * Intel counters need root access
	 */

	Intel_PCM::safe_pcm_state();

	hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::parallel>(field, output_field, 20);

	Intel_PCM::add_pcm_to_category("Hoshen_Kopelman with union find");
	Statistic::print_data();

	//save_benchmark_results(benchmarkOutputArg.getValue(), benchmark_results);

	if (outputArg.getValue() == "STDOUT") {
		std::cout << "Input:" << std::endl;
		print_lattice_stdout(field);
		std::cout << "Output:" << std::endl;
		print_lattice_stdout(output_field);
	} else {
		//export ppm/png file
		print_latticeV2(output_field,m,n,outputArg.getValue());
	}

	return 0;
}
#include "datastructures/statistic.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>

Statistic::category_cont_t Statistic::data;


void Statistic::add_to_category(const key_t category, const key_t name, const value_t value) {
	//check if already existing
	data[category][name](value);

	/*
	if(data[category].find(name) != data[category].end()){
		data[category][name](value);
	}else{
		data[category].emplace(name, list_t());
		data[category][name](value);
	}*/
}

void Statistic::print_data() {
	for(auto& cat : data){
		std::cout << cat.first << std::setw(15) << "mean" << std::setw(15) <<  "var" << std::setw(15) <<  "min"<<  std::setw(15) <<  "min" << std::endl;
		for(auto& loc_data : cat.second){
			std::cout << "└"<< " " << loc_data.first << std::setw(15) << acc::mean(loc_data.second) << std::setw(15) << acc::variance(loc_data.second)
			<< std::setw(15) << acc::min(loc_data.second) << std::setw(15) << acc::max(loc_data.second) <<std::endl;
		}
		std::cout << std::endl;
	}
}

std::string quotation(const std::string & in){
	return std::string("\"") + in + std::string("\": ");
}

void Statistic::json_export(const key_t file_name) {
	std::ofstream myfile;
	myfile.open(file_name + key_t(".json"), std::ios::out | std::ios::trunc);

	myfile << "{" << std::endl;
	unsigned int outer_count = 1;
	for(auto& cat : data){
		unsigned int count = 1;
		myfile << quotation(cat.first) << "{" <<std::endl;
		for(auto& loc_data : cat.second){
			myfile << quotation(loc_data.first) <<  "{" <<std::endl;
			myfile << quotation("mean") << acc::mean(loc_data.second) <<", " << std::endl;
			myfile << quotation("var") << acc::variance(loc_data.second) <<", " << std::endl;
			myfile << quotation("min") << acc::min(loc_data.second) <<", " << std::endl;
			myfile << quotation("max") << acc::max(loc_data.second) <<std::endl;
			if(count < cat.second.size())
				myfile << "}," <<std::endl;
			else
				myfile << "}" <<std::endl;
			++count;
		}
		if(outer_count < data.size())
			myfile << "}," << std::endl;
		else
			myfile << "}" << std::endl;
		++outer_count;
	}
	myfile << "}" << std::endl;
	myfile.close();
}



/*************************************************************************
 * INTEL PCM
 *************************************************************************/


// save state to statistic if macro defined, empty functions else
#ifdef ACTIVATE_INTEL_PCM
SystemCounterState Intel_PCM::state;

void Intel_PCM::add_pcm_to_category(const Statistic::key_t category) {
	SystemCounterState after_state = getSystemCounterState();

	Statistic::add_to_category(category, "Instructions per clock ", getIPC(state,after_state) );
	Statistic::add_to_category(category, "Bytes read ", getBytesReadFromMC(state, after_state));
	Statistic::add_to_category(category, "L2 cache hit ratio ", getL2CacheHitRatio(state, after_state));
	Statistic::add_to_category(category, "L3 cache hit ratio ", getL3CacheHitRatio(state, after_state));

}
void Intel_PCM::safe_pcm_state() {
	state = getSystemCounterState();
}

#else
void Intel_PCM::add_pcm_to_category(const Statistic::key_t category) {}
void Intel_PCM::safe_pcm_state() {}

#endif
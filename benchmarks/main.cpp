#include <iomanip>

#include "profiler.hpp"

#include "types.hpp"
#include "utilities.hpp"
#include "type/type.hpp"

#include "benchmark_helper.hpp"

#include "arguments.hpp"
#include "kernel_collection.hpp"
#include "runner/single_core_runner.hpp"
#include "multi_core.hpp"

#include "hoshen_fsm.hpp"

#include "hoshen_pthreads.hpp"
#include "hoshen_uf_pthreads.hpp"

#include <boost/property_tree/json_parser.hpp>

#include "hoshen_pthreads.hpp"

#include <boost/mpl/transform.hpp>
#include <boost/mpl/joint_view.hpp>
#include <boost/mpl/vector.hpp>

#include <boost/property_tree/ptree.hpp>

template <typename T>
struct csize_node_transformer {
	using type = CSizeNode<T>;
};

template <typename FIELD_T, typename LABEL_T>
struct kernel_template_argument_transformer {
	using type = mpl::vector<FIELD_T, LABEL_T>;
};

using namespace boost;

using boost::property_tree::ptree;
using boost::property_tree::json_parser::write_json;

/*static void bench_bool_vector_access(BenchmarkState<hoshen_bench_arg>& state) {
	constexpr size_t num_bytes = 50 * 1000 * 1000;

	std::vector<bool> field(num_bytes*8, 0);

	while (state.keep_running()) {
		escape(field);
		BENCH_TIMER_START(state)
		for (size_t i=0; i<num_bytes*8; ++i) {
			escape(field[i]);
			if (__builtin_expect(field[i]==0, 1))
				continue;

			field[i] = 1;
			escape(field[i]);
		}
		BENCH_TIMER_STOP(state)
		escape(field);
	}

	state.set_attr("mb / s", state.iterations*num_bytes/(1000*1000*sum(state.time_accumulator)));
}*/

// Get current date/time
const std::string currentDateTime() {
	std::time_t t = std::time(nullptr);
	std::tm tm = *std::localtime(&t);

    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y_%m_%d__%H_%M_%S", &tstruct);

    //std::stringstream time_ss;
    //time_ss << std::put_time(&tm, "%Y_%m_%d__%H_%M_%S");

    return buf;
}


static void write_benchmark_results(ptree results) {
	// serialize
	std::stringstream ss;
    write_json(ss, results);

    // determine filename
    static std::string time_string = currentDateTime();

    // write to file
    std::ofstream fs;
	fs.open("benchmark_results_"+time_string+".json");

	fs << ss.str();

	fs.close();
}

int main () {
	// Initialize profiler
	Profiler profiler;

	// typedefs
	/*typedef BenchmarkState<hoshen_bench_arg> benchmark_state_t;
	std::cout << benchmark_state_t::header() << std::endl;

	using store_types = mpl::vector<int32_t, int64_t>;
	using field_types = mpl::vector<int32_t, int64_t, int32_t, int64_t>;
	using label_types = mpl::joint_view<store_types, mpl::transform<store_types, csize_node_transformer<mpl::_1>>::type>::type;

	using kernel_template_arguments = mpl::transform<field_types, label_types, kernel_template_argument_transformer<mpl::_1, mpl::_2>>::type;

	HoshenBenchmarkSingleCoreRunner hoshen_benchmark_single_core(profiler);

	hoshen_benchmark_single_core.run<kernel_template_arguments>(
		std::array<std::string, 2>{"hoshen_uf", "hoshen_fsm"},
		hoshen_bench_arg_mid_low()
	);

	using mc_kernel_template_arguments = mpl::transform<store_types, mpl::transform<store_types, csize_node_transformer<mpl::_1>>::type, kernel_template_argument_transformer<mpl::_1, mpl::_2>>::type;

	//HoshenBenchmarkMultiCoreRunner hoshen_benchmark_multi_core(profiler, hoshen_bench_arg_mid_high());

	//hoshen_benchmark_multi_core.run<mc_kernel_template_arguments>();*/

	/*
	 * Run benchmark for different propabilities
	 */
	/*kernel_collection<Matrix<char>, Matrix<int64_t>, int64_t> all_kernels;

	for (auto kernel : all_kernels()) {
		if (kernel.first == "hoshen_fsm")
			continue;
		for (auto arg : hoshen_bench_arg_prop()) {
			auto result = profiler.benchmark(
				arg,
				std::bind(bench_hoshen_single_core<char, int64_t, int64_t>, kernel.second, std::placeholders::_1)
			);
			// add name of the kernel
			result.set_attr("kernel", kernel.first);
			// print results to the console
			std::cout << result << std::endl;
		}
	}*/

	ptree benchmark_results;

	ptree parallel_benchmark_results;

	auto update_benchmark_results = [&benchmark_results] (std::string section, ptree results) {
		benchmark_results.put_child(section, results);
		write_benchmark_results(benchmark_results);
	};

	/*
	 * Run multicore benchmarks
	 */
	constexpr size_t max_num_threads = 24;

	std::cout << "------------------------------------------------------------" << std::endl
			  << " Running baseline benchmark" << std::endl
			  << "------------------------------------------------------------" << std::endl;
	std::cout << BenchmarkState<hoshen_bench_arg>::header() << std::endl;
	auto baseline_argument = hoshen_bench_arg{16000, 16000, 0.59};
	auto kernel = static_cast<UnionFind<store_t, store_t>(*)(const Matrix<bool>&, Matrix<store_t>&)>(hoshen_uf<Matrix<bool>, Matrix<store_t>, store_t>);
	auto baseline_result = profiler.benchmark(
		baseline_argument,
		std::bind(
			bench_hoshen_single_core<bool, store_t, store_t>,
			kernel,
			std::placeholders::_1
		)
	);
	baseline_result.set_attr("kernel", "hoshen_uf");
	std::cout << baseline_result << std::endl;
	update_benchmark_results("parallel_baseline",  baseline_result.properties());

	std::cout << "------------------------------------------------------------" << std::endl
			  << " Running parallel benchmark" << std::endl
			  << "------------------------------------------------------------" << std::endl;
	std::cout << BenchmarkState<hoshen_bench_parallel_arg>::header() << std::endl;
	for (unsigned num_threads=1; num_threads<max_num_threads+1; ++num_threads) {
		auto arg = hoshen_bench_parallel_arg{num_threads, baseline_argument.rows, baseline_argument.cols, baseline_argument.p, baseline_result};
		auto kernel = static_cast<void(*)(const Matrix<bool>&, Matrix<store_t>&, size_t)>(hoshen_kopel_pthreads);
		auto result = profiler.benchmark(
			arg,
			std::bind(
				bench_hoshen_multi_core<decltype(kernel), bool, store_t, CSizeNode<store_t>>,
				kernel,
				num_threads,
				std::placeholders::_1
			)
		);
		result.set_attr("kernel", "hoshen_kopel_pthreads");
		std::cout << result << std::endl;
		parallel_benchmark_results.add_child("", result.properties());
		update_benchmark_results("parallel", parallel_benchmark_results);
	}


	std::cout << std::endl;

	for (unsigned num_threads=1; num_threads<max_num_threads+1; ++num_threads) {
		auto arg = hoshen_bench_parallel_arg{num_threads, baseline_argument.rows, baseline_argument.cols, baseline_argument.p, baseline_result};
		auto kernel = hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::serial, Matrix<bool>, Matrix<store_t>>;
		auto result = profiler.benchmark(
			arg,
			std::bind(
				bench_hoshen_multi_core<decltype(kernel), bool, store_t, CSizeNode<store_t>>,
				kernel,
				num_threads,
				std::placeholders::_1
			)
		);
		result.set_attr("kernel", "hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::serial>");
		std::cout << result << std::endl;
		parallel_benchmark_results.add_child("", result.properties());
		update_benchmark_results("parallel", parallel_benchmark_results);
	}

	for (unsigned num_threads=1; num_threads<max_num_threads+1; ++num_threads) {
		auto arg = hoshen_bench_parallel_arg{num_threads, baseline_argument.rows, baseline_argument.cols, baseline_argument.p, baseline_result};
		auto kernel = hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::parallel, Matrix<bool>, Matrix<store_t>>;
		auto result = profiler.benchmark(
			arg,
			std::bind(
				bench_hoshen_multi_core<decltype(kernel), bool, store_t, CSizeNode<store_t>>,
				kernel,
				num_threads,
				std::placeholders::_1
			)
		);
		result.set_attr("kernel", "hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::parallel>");
		std::cout << result << std::endl;
		parallel_benchmark_results.add_child("", result.properties());
		update_benchmark_results("parallel", parallel_benchmark_results);
	}

	for (unsigned num_threads=1; num_threads<max_num_threads+1; ++num_threads) {
		auto arg = hoshen_bench_parallel_arg{num_threads, baseline_argument.rows, baseline_argument.cols, baseline_argument.p, baseline_result};
		auto kernel = hoshen_kopel_uf_pthreads<Geometry::tiles, Merge_Type::parallel, Matrix<bool>, Matrix<store_t>>;
		auto result = profiler.benchmark(
			arg,
			std::bind(
				bench_hoshen_multi_core<decltype(kernel), bool, store_t, CSizeNode<store_t>>,
				kernel,
				num_threads,
				std::placeholders::_1
			)
		);
		result.set_attr("kernel", "hoshen_kopel_uf_pthreads<Geometry::tiles, Merge_Type::parallel>");
		std::cout << result << std::endl;
		parallel_benchmark_results.add_child("", result.properties());
		update_benchmark_results("parallel", parallel_benchmark_results);
	}

	//hoshen_kopel_pthreads
	//hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::serial>
	//hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::parallel>
	//hoshen_kopel_uf_pthreads<Geometry::tiles, Merge_Type::serial>
	//hoshen_kopel_uf_pthreads<Geometry::tiles, Merge_Type::parallel>
}
#include <vector>
#include <array>

#include "profiler.hpp"

struct hoshen_bench_arg {
	size_t rows;
	size_t cols;
	double p;

	//BenchmarkState<> baseline;

	static std::array<std::string, 10> header_attributes() {
		return {
			"kernel",
			"rows",
			"cols",
			"p",
			"field_store_t",
			"output_store_t",
			"label_t",
			"labels",
			"items / s",
			"mb / s"
		};
	}
};

struct hoshen_bench_parallel_arg {
	size_t num_threads;
	size_t rows;
	size_t cols;
	double p;

	BenchmarkState<hoshen_bench_arg> baseline;

	static std::array<std::string, 9> header_attributes() {
		return {
			"kernel",
			"rows",
			"cols",
			"p",
			"labels",
			"items / s",
			"mb / s",
			"speedup",
			"num_threads"
		};
	}
};

static std::vector<hoshen_bench_arg> hoshen_bench_arg_prop() {
	std::vector<hoshen_bench_arg> args;
	
    for (size_t i=0; i<100; ++i) {
    	args.emplace_back(hoshen_bench_arg{16384, 16384, i/100.});
    }

	return args;
}

static std::vector<hoshen_bench_arg> hoshen_bench_arg_low() {
	double p = 0.59;
	std::vector<hoshen_bench_arg> args;

	args.emplace_back(hoshen_bench_arg{4, 4, p});
    for (size_t j = 32; j <= 256; j += 32)
      args.emplace_back(hoshen_bench_arg{j, j, p});

	return args;
}

static std::vector<hoshen_bench_arg> hoshen_bench_arg_mid() {
	double p = 0.59;
	std::vector<hoshen_bench_arg> args;

    for (size_t j = 256; j <= 2048; j *= 2)
      args.push_back(hoshen_bench_arg{j, j, p});

	return args;
}

static std::vector<hoshen_bench_arg> hoshen_bench_arg_mid_high() {
	double p = 0.59;
	std::vector<hoshen_bench_arg> args;

    for (size_t j = 4096; j <= 16384; j *= 2)
      args.push_back(hoshen_bench_arg{j, j, p});

	return args;
}

static std::vector<hoshen_bench_arg> hoshen_bench_arg_high() {
	double p = 0.59;
	std::vector<hoshen_bench_arg> args;

    for (size_t j = 8096; j <= 65536; j *= 2)
      args.push_back(hoshen_bench_arg{j, j, p});

	return args;
}
#include "profiler.hpp"
#include "datastructures/matrix.hpp"

template <typename KERNEL, typename FIELD_STORE_T, typename OUTPUT_STORE_T, typename LABEL_T>
static void bench_hoshen_multi_core(KERNEL kernel, size_t num_threads, BenchmarkState<hoshen_bench_parallel_arg>& state) {
        size_t m = state.argument.rows;
        size_t n = state.argument.cols;
        auto p = state.argument.p;
    size_t bytes_processed = 0;
    size_t csize_size_acc = 0;

    assert(m >= 256 && n >= 256);

    Matrix<FIELD_STORE_T> field(m, n);
    create_perc(field, p, 42);

    while (state.keep_running()) {
        // create input data
        Matrix<OUTPUT_STORE_T> output(m, n);
        clear_cpu_cache();

        // run
	    BENCH_TIMER_START(state)
        kernel(field, output, num_threads);
        escape(output);
        BENCH_TIMER_STOP(state)

        // calc bytes process in this iteration
        //bytes_processed += calc_bytes_processed(field, output, csize);
        //csize_size_acc += csize.size();
        clear_cpu_cache();
    }

    //state.set_attr("bytes_processed", bytes_processed);
    state.set_attr("mb / s", bytes_processed/(1000*1000.*sum(state.time_accumulator)));
    state.set_attr("field_store_t", type_printer::type<FIELD_STORE_T>());
    state.set_attr("output_store_t", type_printer::type<OUTPUT_STORE_T>());
    state.set_attr("label_t", type_printer::type<LABEL_T>());
    state.set_attr("items / s", state.iterations * m * n / sum(state.time_accumulator));
    state.set_attr("labels", double(csize_size_acc)/state.iterations);
    state.set_attr("rows", m);
    state.set_attr("cols", n);
    state.set_attr("p", p);
    state.set_attr("num_threads", num_threads);
    state.set_attr("speedup", mean(state.argument.baseline.time_accumulator)/mean(state.time_accumulator));
}
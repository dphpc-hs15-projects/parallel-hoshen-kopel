#include <cstring>

#include "datastructures/matrix.hpp"

#define CACHE_SIZE 8192*1024

enum BenchmarkCacheState : bool {
    warm,
    cold
};

template <typename T1, typename T2, typename CSIZE_T>
static size_t calc_bytes_processed(const calc_t p, const Matrix<T1>& field, const Matrix<T2>& output, const CSIZE_T& csize) {
    size_t bytes_processed = 0;

    // field
    if (std::is_same<T1, bool>::value) {
        bytes_processed += field.m*field.n; // std::vector<bool> is specialized
        bytes_processed += output.m*output.n*sizeof(T2)*p;
    } else if (field != output) {
        bytes_processed += field.m*field.n*sizeof(T1);
        bytes_processed += output.m*output.n*sizeof(T2)*p;
    } else {
        bytes_processed += output.m*output.n*sizeof(T2);
    }

    // csize
    bytes_processed += csize.size()*sizeof(typename CSIZE_T::value_type);

    return bytes_processed;
}

static void clear_cpu_cache() {
    char* dummy = new char[CACHE_SIZE];
    memset (dummy, 0, CACHE_SIZE);
    escape(dummy);
    delete[] dummy;
}
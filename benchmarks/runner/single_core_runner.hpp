#include <vector>
#include <utility>

#include <boost/mpl/vector.hpp>
#include <boost/mpl/for_each.hpp>

#include "profiler.hpp"

#include "kernel_collection.hpp"
#include "../single_core.hpp"


using namespace boost;

struct HoshenBenchmarkSingleCoreRunner
{
	using argument_container_t = std::vector<hoshen_bench_arg>;

	Profiler& profiler;

	argument_container_t args;

    std::vector<std::string> kernels;

	HoshenBenchmarkSingleCoreRunner(Profiler& profiler_) : profiler(profiler_) {}

	// this is the entry point for the types of the cross product (their interface sucks...)
    template <typename kernel_template_arguments>
    void operator()(kernel_template_arguments) const
    {
    	using field_store_t = bool;
    	using output_store_t = typename mpl::at<kernel_template_arguments, mpl::int_<0>>::type;
    	using label_t = typename mpl::at<kernel_template_arguments, mpl::int_<1>>::type;

    	kernel_collection<Matrix<field_store_t>, Matrix<output_store_t>, label_t> all_kernels;

    	for (auto arg : args) {
    		for (auto kernel : all_kernels()) {
                if (std::find(kernels.begin(), kernels.end(), kernel.first) == kernels.end())
                    continue;

    			// run benchmark
	    		auto result = profiler.benchmark(
					arg,
					std::bind(bench_hoshen_single_core<field_store_t, output_store_t, label_t>, kernel.second, std::placeholders::_1)
				);
				// add name of the kernel
				result.set_attr("kernel", kernel.first);
				// print results to the console
				std::cout << result << std::endl;
			}
			std::cout << std::endl;
    	}
    }

    template <typename kernel_template_arguments_vector, size_t NUM_KERNELS>
    void run(std::array<std::string, NUM_KERNELS> kernels_, const argument_container_t& args_) {
        kernels = std::vector<std::string>(kernels_.begin(), kernels_.end());
        args = args_;

    	// run benchmark with all kernel_template arguments
    	mpl::for_each< kernel_template_arguments_vector >( *this );
    }
};
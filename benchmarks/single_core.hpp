#ifndef BENCH_SINGLE_CORE_HPP
#define BENCH_SINGLE_CORE_HPP
#include "profiler.hpp"
#include "datastructures/matrix.hpp"

template <typename FIELD_STORE_T, typename OUTPUT_STORE_T, typename LABEL_T, BenchmarkCacheState cache_state = BenchmarkCacheState::cold>
static void bench_hoshen_single_core(UnionFind<OUTPUT_STORE_T, LABEL_T>(*kernel)(const Matrix<FIELD_STORE_T>&, Matrix<OUTPUT_STORE_T>&), BenchmarkState<hoshen_bench_arg>& state) {
	size_t m = state.argument.rows;
	size_t n = state.argument.cols;
	auto p = state.argument.p;
    size_t bytes_processed = 0;
    size_t csize_size_acc = 0;

    assert(cache_state == BenchmarkCacheState::cold || (m >= 256 && n >= 256));

    Matrix<FIELD_STORE_T> field(m, n);
    create_perc(field, p, 42);

    while (state.keep_running()) {
        // create input data
        Matrix<OUTPUT_STORE_T> output(m, n);

        if (cache_state == BenchmarkCacheState::cold) {
            clear_cpu_cache();
        }

        // run
        BENCH_TIMER_START(state)
    	auto csize = kernel(field,output);
        escape(csize);
        BENCH_TIMER_STOP(state)

        // calc bytes process in this iteration
        bytes_processed += calc_bytes_processed(p, field, output, csize);
        csize_size_acc += csize.size();
        if (cache_state == BenchmarkCacheState::cold) clear_cpu_cache();
    }

    state.set_attr("mb / s", bytes_processed/(1000*1000.*sum(state.time_accumulator)));
    state.set_attr("field_store_t", type_printer::type<FIELD_STORE_T>());
    state.set_attr("output_store_t", type_printer::type<OUTPUT_STORE_T>());
    state.set_attr("label_t", type_printer::type<LABEL_T>());
    state.set_attr("items / s", state.iterations * m * n / sum(state.time_accumulator));
    state.set_attr("labels", double(csize_size_acc)/state.iterations);
    state.set_attr("rows", m);
    state.set_attr("cols", n);
    state.set_attr("p", p);

}
#endif
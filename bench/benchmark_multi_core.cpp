#include "benchmark/benchmark.h"
#include "types.hpp"
#include "datastructures/matrix.hpp"
#include "utilities.hpp"
#include "type/type.hpp"

#include "benchmark_helper.hpp"

// algorithms
#include "hoshen_naive.hpp"
#include "hoshen_uf.hpp"
#include "hoshen_pthreads.hpp"
#include "hoshen_uf_pthreads.hpp"

static double p = 0.59;

template <typename FIELD_STORE_T, typename OUTPUT_STORE_T, void(*KERNEL)(const Matrix<FIELD_STORE_T>&, Matrix<OUTPUT_STORE_T>&, size_t), BenchmarkCacheState cache_state = BenchmarkCacheState::warm>
static void BM_Hoshen_multicore(benchmark::State& state) {
    size_t N = state.range_x();
    size_t num_threads = state.range_y();
    size_t bytes_processed = 0;
    size_t csize_size_acc = 0;
    size_t redirections = 0;

    assert(cache_state == BenchmarkCacheState::cold || N >= 256);

    Matrix<FIELD_STORE_T> field(N, N);
    Matrix<OUTPUT_STORE_T> output(N, N);
    create_perc(field, p);

    if (cache_state == BenchmarkCacheState::cold)
        clear_cpu_cache();

    while (state.KeepRunning()) {
        // run
        auto result = KERNEL(field, output, num_threads); //<decltype(field), decltype(output)>
        escape(result);

        // calc bytes process in this iteration
        state.PauseTiming();
        //bytes_processed += calc_bytes_processed(field, output, csize);
        //csize_size_acc += csize.size();
        //redirections += csize.redirections;
        if (N >= 256 && cache_state == BenchmarkCacheState::cold) clear_cpu_cache();
        state.ResumeTiming();
    }

    /*state.SetLabel("p: " + std::to_string(p) 
        + ", redirections: " + std::to_string(redirections/state.iterations())
        + ", avg csize: " + std::to_string(csize_size_acc/state.iterations())
    );*/
    state.SetLabel("p: " + std::to_string(p)
        + ", num_threads: " + std::to_string(num_threads)
        + ", field_size: " + std::to_string(N)
        + ", field_store_t: " + type<FIELD_STORE_T>()
        + ", output_val_t: " + type<OUTPUT_STORE_T>()
    );
    state.SetItemsProcessed(state.iterations() * N*N);
    //state.SetBytesProcessed(bytes_processed);
}

template <typename FIELD_STORE_T, typename OUTPUT_STORE_T, BenchmarkCacheState cache_state = BenchmarkCacheState::warm>
static void BM_Hoshen_global_uf_multicore(benchmark::State& state) {
    size_t N = state.range_x();
    size_t num_threads = state.range_y();
    size_t bytes_processed = 0;
    size_t csize_size_acc = 0;
    size_t redirections = 0;

    assert(cache_state == BenchmarkCacheState::cold || N >= 256);

    Matrix<FIELD_STORE_T> field(N, N);
    create_perc(field, p);

    //if (cache_state == BenchmarkCacheState::cold)
    //    clear_cpu_cache();

    while (state.KeepRunning()) {
        Matrix<OUTPUT_STORE_T> output(N, N);
        // run
        //hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::parallel>(field,output, num_threads); //<decltype(field), decltype(output)>
        hoshen_kopel_pthreads(field,output, num_threads);
        escape(output);

        // calc bytes process in this iteration
        //state.PauseTiming();
        //bytes_processed += calc_bytes_processed(field, output, csize);
        //csize_size_acc += csize.size();
        //redirections += csize.redirections;
        //if (N >= 256 && cache_state == BenchmarkCacheState::cold) clear_cpu_cache();
        //state.ResumeTiming();
    }

    /*state.SetLabel("p: " + std::to_string(p) 
        + ", redirections: " + std::to_string(redirections/state.iterations())
        + ", avg csize: " + std::to_string(csize_size_acc/state.iterations())
    );*/
    state.SetLabel("p: " + std::to_string(p)
        + ", num_threads: " + std::to_string(num_threads)
        + ", field_size: " + std::to_string(N)
        + ", field_store_t: " + type<FIELD_STORE_T>()
        + ", output_val_t: " + type<OUTPUT_STORE_T>()
    );
    state.SetItemsProcessed(state.iterations() * N*N);
    //state.SetBytesProcessed(bytes_processed);
}

static std::vector<size_t> hoshen_mid_high_field_sizes() {
    std::vector<size_t> field_sizes;
    for (size_t field_size = 4096; field_size <= 32768; field_size *= 2) {
        field_sizes.push_back(field_size);
    }
    return field_sizes;
}

static void Hoshen_Argument_Mid_High(benchmark::internal::Benchmark* b) {
    auto field_sizes = hoshen_mid_high_field_sizes();
    for (size_t field_size : field_sizes)
        for (size_t num_threads = 1; num_threads < 2; ++num_threads)
            b = b->ArgPair(field_size, num_threads);
}

// Mid_High
//BENCHMARK_TEMPLATE(BM_Hoshen_multicore, bool, store_t, hoshen_kopel_pthreads, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High)
//    ->UseRealTime();

/*BENCHMARK_TEMPLATE(BM_Hoshen_multicore, bool, store_t, static_cast<UnionFindGlobal_ThreadSafe<Geometry::stripes>(*)(const Matrix<bool>&, Matrix<store_t>&, size_t)>(hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::parallel>), BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High)
    ->UseRealTime();*/

BENCHMARK_TEMPLATE(BM_Hoshen_global_uf_multicore, bool, store_t, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High)
    ->UseRealTime();

/*BENCHMARK_TEMPLATE(BM_Hoshen_baseline, bool, store_t, hoshen_kopel_uf_pthreads, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High_)
    ->UseRealTime();*/
//BENCHMARK_TEMPLATE(BM_Hoshen_uf, bool, store_t, BenchmarkCacheState::cold)->Apply(Hoshen_Argument_Mid);

// Mid-High range
//BENCHMARK_TEMPLATE(BM_Hoshen_uf, bool, store_t, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High);
//BENCHMARK_TEMPLATE(BM_Hoshen_uf, bool, store_t, BenchmarkCacheState::cold)->Apply(Hoshen_Argument_Mid_High);

BENCHMARK_MAIN();
#include "benchmark/benchmark.h"
#include "types.hpp"
#include "datastructures/matrix.hpp"
#include "utilities.hpp"

#include "benchmark_helper.hpp"

// algorithms
#include "hoshen_naive.hpp"
#include "hoshen_uf.hpp"
#include "hoshen_kopel_fsm.hpp"

static double p = 0.59;

template <typename FIELD_STORE_T, typename OUTPUT_STORE_T, typename CSIZE_STORE_T, BenchmarkCacheState cache_state = BenchmarkCacheState::warm>
static void BM_Hoshen_naive(benchmark::State& state) {
    size_t N = state.range_x();
    size_t bytes_processed = 0;
    size_t csize_size_acc = 0;

    assert(cache_state == BenchmarkCacheState::cold || N >= 256);

    Matrix<FIELD_STORE_T> field(N, N, 42);
    create_perc(field, p);

    while (state.KeepRunning()) {
        // create input data
        state.PauseTiming();
        Matrix<OUTPUT_STORE_T> output(N, N);

        if (cache_state == BenchmarkCacheState::cold) {
            clear_cpu_cache();
        }
        state.ResumeTiming();

        // run
    	auto csize = hoshen_naive<decltype(field), decltype(output), CSIZE_STORE_T>(field,output);
        escape(csize);

        // calc bytes process in this iteration
        state.PauseTiming();
        bytes_processed += calc_bytes_processed(field, output, csize);
        csize_size_acc += csize.size();
        if (N >= 256) clear_cpu_cache();
        state.ResumeTiming();
    }

    state.SetLabel("p: " + std::to_string(p) + ", avg csize: " + std::to_string(csize_size_acc/state.iterations()));
    state.SetItemsProcessed(state.iterations() * N*N);
    state.SetBytesProcessed(bytes_processed);
}

template <typename FIELD_STORE_T, typename OUTPUT_STORE_T, typename CSIZE_STORE_T, BenchmarkCacheState cache_state = BenchmarkCacheState::warm>
static void BM_Hoshen_fsm(benchmark::State& state) {
    size_t N = state.range_x();
    size_t bytes_processed = 0;
    size_t csize_size_acc = 0;

    assert(cache_state == BenchmarkCacheState::cold || N >= 256);

    Matrix<FIELD_STORE_T> field(N, N);
    create_perc(field, p, 42);

    while (state.KeepRunning()) {
        // create input data
        state.PauseTiming();
        Matrix<OUTPUT_STORE_T> output(N, N);

        if (cache_state == BenchmarkCacheState::cold) {
            clear_cpu_cache();
        }
        state.ResumeTiming();

        // run
        auto csize = hoshen_kopel_fsm<decltype(field), decltype(output), CSIZE_STORE_T>(field, output);
        escape(csize);

        // calc bytes process in this iteration
        state.PauseTiming();
        bytes_processed += calc_bytes_processed(field, output, csize);
        csize_size_acc += csize.size();
        if (N >= 256) clear_cpu_cache();
        state.ResumeTiming();
    }

    state.SetLabel("p: " + std::to_string(p) + ", avg csize: " + std::to_string(csize_size_acc/state.iterations()));
    state.SetItemsProcessed(state.iterations() * N*N);
    state.SetBytesProcessed(bytes_processed);
}

template <typename FIELD_STORE_T, typename OUTPUT_STORE_T, typename LABEL_T, UnionFind<OUTPUT_STORE_T, LABEL_T>(*KERNEL)(const Matrix<FIELD_STORE_T>&, Matrix<OUTPUT_STORE_T>&), BenchmarkCacheState cache_state = BenchmarkCacheState::warm>
static void BM_Hoshen(benchmark::State& state) {
    size_t N = state.range_x();
    size_t bytes_processed = 0;
    size_t csize_size_acc = 0;
    size_t redirections = 0;

    assert(cache_state == BenchmarkCacheState::cold || N >= 256);

    while (state.KeepRunning()) {
        // create input data
        state.PauseTiming();
        Matrix<FIELD_STORE_T> field(N, N);
        Matrix<OUTPUT_STORE_T> output(N, N);
        create_perc(field, p);

        if (cache_state == BenchmarkCacheState::cold) {
            clear_cpu_cache();
        }
        state.ResumeTiming();

        // run
        auto csize = KERNEL(field,output); //<decltype(field), decltype(output)>
        asm volatile("" :  : "g"(csize) : "memory");

        // calc bytes process in this iteration
        state.PauseTiming();
        bytes_processed += calc_bytes_processed(field, output, csize);
        csize_size_acc += csize.size();
        redirections += csize.redirections;
        if (N >= 256) clear_cpu_cache();
        state.ResumeTiming();
    }

    state.SetLabel("p: " + std::to_string(p) 
        + ", redirections: " + std::to_string(redirections/state.iterations())
        + ", avg csize: " + std::to_string(csize_size_acc/state.iterations())
    );
    state.SetItemsProcessed(state.iterations() * N*N);
    state.SetBytesProcessed(bytes_processed);
}

static void Hoshen_Argument_Low(benchmark::internal::Benchmark* b) {
    b = b->Arg(4); // baseline
    for (size_t j = 32; j <= 32; j += 32)
      b = b->Arg(j);
}

static void Hoshen_Argument_Mid(benchmark::internal::Benchmark* b) {
    for (size_t j = 256; j <= 2048; j *= 2)
      b = b->Arg(j);
    //b = b->Arg(4096);
}

static void Hoshen_Argument_Mid_High(benchmark::internal::Benchmark* b) {
    for (size_t j = 4096; j <= 16384; j *= 2)
      b = b->Arg(j);
}

// Low range
//BENCHMARK_TEMPLATE(BM_Hoshen_naive, bool, store_t, store_t, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Low);

// Mid range
//BENCHMARK_TEMPLATE(BM_Hoshen_naive, bool, store_t, store_t, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid);
//BENCHMARK_TEMPLATE(BM_Hoshen_naive, bool, store_t, store_t, BenchmarkCacheState::cold)->Apply(Hoshen_Argument_Mid);
//BENCHMARK_TEMPLATE(BM_Hoshen_naive, bool, store_t, CSizeNode<store_t>)->Apply(Hoshen_Argument_Mid);

//BENCHMARK_TEMPLATE(BM_Hoshen_uf, bool, store_t, hoshen_kopel_uf_optimized, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid);
BENCHMARK_TEMPLATE(BM_Hoshen, bool, store_t, store_t, hoshen_kopel_uf, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High)
    ->UseRealTime();

BENCHMARK_TEMPLATE(BM_Hoshen, bool, store_t, CSizeNode<store_t>, hoshen_kopel_uf, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High)
    ->UseRealTime();

BENCHMARK_TEMPLATE(BM_Hoshen, bool, store_t, store_t, hoshen_kopel_fsm, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High)
    ->UseRealTime();

BENCHMARK_TEMPLATE(BM_Hoshen, bool, store_t, CSizeNode<store_t>, hoshen_kopel_fsm, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High)
    ->UseRealTime();
//BENCHMARK_TEMPLATE(BM_Hoshen_uf, bool, store_t, BenchmarkCacheState::cold)->Apply(Hoshen_Argument_Mid);

// Mid-High range
//BENCHMARK_TEMPLATE(BM_Hoshen_uf, bool, store_t, BenchmarkCacheState::warm)->Apply(Hoshen_Argument_Mid_High);
//BENCHMARK_TEMPLATE(BM_Hoshen_uf, bool, store_t, BenchmarkCacheState::cold)->Apply(Hoshen_Argument_Mid_High);

BENCHMARK_MAIN();
#ifndef TYPE_HPP
#define TYPE_HPP

#include <string>
#include <typeinfo>

namespace type_printer {

std::string demangle(const char* name);

template <class T>
std::string type() {
	T dummy;
    return demangle(typeid(dummy).name());
}

}

#endif
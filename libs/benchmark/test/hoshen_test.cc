#include "benchmark/benchmark.h"
#include "datastructures/matrix.hpp"
#include "datastructures/region.hpp"
#include "types.hpp"

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <memory>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <list>
#include <map>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>


template<typename T>
void create_perc(T& field, calc_t const& p){

	static std::mt19937_64 rand_gen(42);
	static std::uniform_real_distribution<calc_t> rand_num(0,1);

	for(size_t i=0; i<field.m; ++i){
		for(size_t j=0; j<field.n; ++j) {
			field(i, j) = rand_num(rand_gen) < p;
		}
	}

}

template<typename T1, typename T2>
int hoshen_naive(const T1& field, T2& output, Region& r, std::shared_ptr<csize_t> csize_ptr=nullptr){
	assert(field.m != 0 && field.n != 0);
	assert(r.mEnd <= field.m);
	assert(r.nEnd <= field.n);

	const size_t m = r.rows();
	const size_t n = r.cols();

	/*#ifdef DEBUG
	static std::mutex debug_mutex;
	debug_mutex.lock();

	std::cout << "id: " << (int) gettid() << " parameters " << nStart << " " << nEnd << " " << mStart << " " << mEnd << std::endl;
	debug_mutex.unlock();
	#endif*/

	size_t k = 2;
	if (csize_ptr == nullptr) {
		csize_ptr = std::make_shared<csize_t>(n*m+3,0);
	}

	csize_t& M(*csize_ptr);

	store_t top,left,current;
	for(size_t i=r.mStart; i<r.mEnd; ++i){
		for(size_t j=r.nStart; j<r.nEnd; ++j){
			current = field(i, j);

			//continue if field is unoccupied
			if(current == 0)
				continue;

			//get top and left label
			top = (i < r.mStart+1) ? 0 : output(i-1, j);
			left = (j < r.nStart+1) ? 0 : output(i, j-1);

			//check labels for connected cluster
			while(M[top] < 0)
				top = - M[top];
			while(M[left] < 0)
				left = - M[left];

			//set current label
			if(top == 0 && left == 0) {
				output(i, j) = k;
				M[k] = 1;
				++k;
			} else if(top != 0 && left == 0) {
				output(i, j) = top;
				M[top] += 1;
			} else if(top == 0) { //left always != 0
				output(i, j) = left;
				M[left] += 1;
			} else if(top == left) {
				output(i, j) = left;
				M[left] += 1;
			} else {
				output(i, j) = top;
				M[top] += M[left]+1;
				M[left] = -top;
			}
		}
	}

	//unify cluster labels
	sint_t label;
	for(size_t i=r.mStart; i<r.mEnd; ++i){
		for(size_t j=r.nStart; j<r.nEnd; ++j){
			label = output(i, j);
			while(M[label] < 0)
				label = - M[label];
			output(i, j) = label;
		}
	}

	int count = 0;
	for (auto& element : M) {
		if 	(element != 0) {
		 ++count;
		}
	}
	return count;
}





static void BM_Hoshen_naive(benchmark::State& state) {
  auto N = state.range_x();
  int count = 0;
  while (state.KeepRunning()) {

    state.PauseTiming();
    double p = 0.6;
    Matrix<bool> field(N, N);
	Matrix<store_t> output_field(N, N);
	create_perc(field, p);
	Region reg(0, field.m, 0, field.n);
    state.ResumeTiming();

    /*
    std::vector<int> count(num_iterations);
    for (int j = 0; j < num_iterations; ++j)
      count[j] = hoshen_naive(field,output_field,reg);
  }
  int total_count = std::accumulate(count.begin(),count.end(),0);
	*/

  	count = hoshen_naive(field,output_field,reg);

  }
  //state.SetItemsProcessed(state.iterations() * num_iterations * (N*N + N*N + count));
  state.SetBytesProcessed(state.iterations() * (N*N*sizeof(store_t) + N*N*sizeof(bool) + count*sizeof(sint_t)));
}

static void CustomArguments(
    benchmark::internal::Benchmark* b) {
    for (int j = 32; j <= 512; j += 32)
      b = b->Arg(j);
}

BENCHMARK(BM_Hoshen_naive)->Apply(CustomArguments);

BENCHMARK_MAIN();
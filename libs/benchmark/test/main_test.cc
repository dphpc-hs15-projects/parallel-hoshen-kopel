#include "benchmark/benchmark.h"
#include "datastructures/matrix.hpp"
#include "types.hpp"
#include "hoshen_pthreads.hpp"
//#include "latticeview.h"


#include <assert.h>
#include <math.h>
#include <stdint.h>

#include <cstdlib>
#include <iostream>
#include <limits>
#include <list>
#include <map>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <vector>


template<typename T>
void create_perc(T& field, calc_t const& p){

	static std::mt19937_64 rand_gen(42);
	static std::uniform_real_distribution<calc_t> rand_num(0,1);

	for(size_t i=0; i<field.m; ++i){
		for(size_t j=0; j<field.n; ++j) {
			field(i, j) = rand_num(rand_gen) < p;
		}
	}

}


namespace {

int main_test(unsigned int num_threads, unsigned int size){

	unsigned int N = size;
	const double p = 0.6;

	Matrix<bool> field(N, N);
	Matrix<store_t> output_field(N, N);
	create_perc(field, p); // create random cluster
	hoshen_kopel_uf_pthreads(field, output_field, num_threads);

	return 0;
}
}


static void BM_main_test (benchmark::State& state) {

	 while (state.KeepRunning()) {
    	main_test(state.range_x(),state.range_y());
	}

}


/*
template<typename T1, typename T2>
int hoshen_test(T1& field, T2& output_field, unsigned int num_threads) {

}
*/
static void BM_hoshen_test (benchmark::State& state) {
	
    
	unsigned int N = state.range_y();
	const double p = 0.6;

	Matrix<bool> field(N, N);
	Matrix<store_t> output_field(N, N);
	create_perc(field, p); // create random cluster



	while(state.KeepRunning()) {
			hoshen_kopel_uf_pthreads(field, output_field, state.range_x());
	}
}



static void CustomArguments(
    benchmark::internal::Benchmark* b) {
  for (int i = 1; i <= 8; ++i)
    for (int j = 2048; j <= 2048; j *= 2)
      b = b->ArgPair(i, j);
}

BENCHMARK(BM_main_test)->Apply(CustomArguments);//->ThreadPerCpu();
BENCHMARK(BM_hoshen_test)->Apply(CustomArguments);

BENCHMARK_MAIN()
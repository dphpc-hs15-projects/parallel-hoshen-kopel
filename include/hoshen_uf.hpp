#ifndef HOSHEN_UF_HPP
#define HOSHEN_UF_HPP

#include "datastructures/union_find.hpp"
#include "datastructures/region.hpp"

template<typename T1, typename T2, typename LABEL_T = typename T2::value_type>
UnionFind<typename T2::value_type, LABEL_T> hoshen_uf(const T1& field, T2& output){
	Region region(0, field.m, 0, field.n);
	auto result = hoshen_uf<T1, T2, LABEL_T>(field, output, region);
	auto& csize = result.csize;

	//unify cluster labels
	store_t label;
	for(size_t i=region.mStart; i<region.mEnd; ++i){
		for(size_t j=region.nStart; j<region.nEnd; ++j){
			label = output(i, j);
			while(csize[label] < 0)
				label = - csize[label];
			output(i, j) = label;
		}
	}

	return result;
}

template<typename T1, typename T2, typename LABEL_T = typename T2::value_type>
UnionFind<typename T2::value_type, LABEL_T> hoshen_uf(const T1& field, T2& output, const Region& region){
	// types
	using label_t = LABEL_T;
	using store_t = typename T2::value_type;
	using uf_t = UnionFind<store_t, label_t>;

	// validate inputs
	assert(field.m != 0);
	assert(field.n != 0);
	assert(field.m == output.m);
	assert(field.n == output.n);
	assert(region.nEnd <= field.n);
	assert(region.mEnd <= field.m);

	const size_t m = region.rows();
	const size_t n = region.cols();

	uf_t partitions(n*m/2 + 3, output);

	store_t top,left;
	bool current;
	for(size_t i=region.mStart; i<region.mEnd; ++i){
		for(size_t j=region.nStart; j<region.nEnd; ++j){
			current = field(i, j);

			//continue if field is unoccupied
			if(current == 0)
				continue;

			//get occupation of top and left field
			top = i<region.mStart + 1 ? 0 : field(i-1, j);
			left = j<region.nStart + 1 ? 0 : field(i, j-1);

			//set current label
			if(top == 0 && left == 0){
				partitions.explore(i, j);
			}else if(top != 0 && left == 0){
				partitions.merge_explore(i, j, i-1, j);
			}else if(top == 0){ //left always != 0
				partitions.merge_explore(i, j, i, j-1);
			}else if((top = partitions.find(i-1, j)) == (left = partitions.find(i, j-1))){
				partitions.merge_explore_by_label(i, j, left);
				//partitions.merge_explore(i, j, i, j-1);
			}else{
				partitions.merge_explore_by_label(i, j, partitions.merge_by_label(top, left));

				//partitions.merge_by_label(top, left);
				//partitions.merge_explore(i, j, i-1, j);

				//partitions.merge(i-1, j, i, j-1); // merge north & west cluster
				//partitions.merge_explore(i, j, i-1, j); // add current field to the newly merged cluster
			}

			//northwest=top;
		}
	}

	return partitions;
}
#endif

#ifndef PROJECT_HIERARCHICAL_MERGE_HPP
#define PROJECT_HIERARCHICAL_MERGE_HPP

#include "datastructures/region_grid.hpp"
#include <list>
#include <thread>

template<typename T1, typename uf_global_t>
void merge_horizontal_border_with_locking(border_t border, T1& field, uf_global_t& union_find_global);

template<typename T1, typename uf_global_t>
void merge_vertical_border_with_locking(border_t border, T1& field, uf_global_t& union_find_global);

template<typename T1, typename uf_global_t>
void hierarchical_merge(T1& field, uf_global_t& union_find_global) {
	auto& region_grid = union_find_global.get_region_grid_ref();
	std::list<std::thread> threads;

	for (border_t border : region_grid.borders()) {
		const auto& r1 = *border.first;
		const auto& r2 = *border.second;
		if (border.first->nStart == border.second->nStart){ //horizontal border
			threads.emplace_back(merge_horizontal_border_with_locking<T1, uf_global_t>, border, std::ref(field), std::ref(union_find_global));
			//merge_horizontal_border_with_locking(border, field, union_find_global);
		} else { // vertical border
			threads.emplace_back(merge_vertical_border_with_locking<T1, uf_global_t>, border, std::ref(field), std::ref(union_find_global));
			//merge_vertical_border_with_locking(border, field, union_find_global);
		}
	}

	for (std::thread& t : threads) {
		t.join();
	}
}

template<typename  T1, typename uf_global_t>
void merge_horizontal_border_with_locking(border_t border, T1& field, uf_global_t& union_find_global) {
	auto& r1 = *border.first;
	auto& r2 = *border.second;

	std::lock(r1.mutex, r2.mutex); // lock deadlock free
	std::lock_guard<std::mutex> lock_guard_1(r1.mutex, std::adopt_lock); //unlock after leaving scope
	std::lock_guard<std::mutex> lock_guard_2(r2.mutex, std::adopt_lock); //unlock after leaving scope

	auto topRow = r1.mEnd - 1;
	for(size_t j = r1.nStart; j < r1.nEnd; ++j) {
		auto top = field(topRow, j);
		auto bottom = field(topRow + 1, j);
		if(top != 0 && bottom != 0) {
			union_find_global.merge(top, r1.index(), bottom, r2.index() );
		}
	}
}

template<typename  T1, typename uf_global_t>
void merge_vertical_border_with_locking(border_t border, T1& field, uf_global_t& union_find_global) {
	auto& r1 = *border.first;
	auto& r2 = *border.second;

	std::lock(r1.mutex, r2.mutex); // lock deadlock free
	std::lock_guard<std::mutex> lock_guard_1(r1.mutex, std::adopt_lock); //unlock after leaving scope
	std::lock_guard<std::mutex> lock_guard_2(r2.mutex, std::adopt_lock); //unlock after leaving scope

	auto leftCol = r1.nEnd - 1;
	// go along border
	for(size_t j = r1.mStart; j < r1.mEnd; ++j) {
		auto top = field(j, leftCol);
		auto bottom = field(j, leftCol + 1);
		if(top != 0 && bottom != 0){
			union_find_global.merge(top, r1.index(), bottom, r2.index());
		}
	}
}

#endif //PROJECT_HIERARCHICAL_MERGE_HPP

#ifndef PROJECT_MERGE_SERIAL_HPP
#define PROJECT_MERGE_SERIAL_HPP

#include <vector>
#include <algorithm>
#include <cassert>
#include <numeric>
#include "types.hpp"
#include "datastructures/region.hpp"

#include "profiler/profiler.hpp"
#include "profiler/rdtsc_timer.hpp"

typedef UnionFind<store_t, CSizeNode<store_t>> merge_serial_uf_t;

//regions must form a regular grid
//regions need to have correct size data
//local labels should be at least 1 and have as maximum at most maxLabel
template<typename T1, Geometry REGION_GRID_GEOMETRY_TYPE>
csize_t merge_serial(T1& field, std::vector<merge_serial_uf_t>& local_regions, RegionGrid<REGION_GRID_GEOMETRY_TYPE>& region_grid);

template<typename T1, typename T2, typename CSIZE_T>
void merge_horizontal_border(Region& r1, Region& r2, CSIZE_T& local_csize1, CSIZE_T& local_csize2, T1& global_csize, T2& field);

template<typename T1, typename T2, typename T3>
void merge_vertical_border(const T1& region, const T1& partner, T2& global_csize, const T3& field);

template<typename T1, typename T2, typename T3>
void write_labels_to_field(T1& field, const T2& region, const T3& global_csize);

template<typename T1, Geometry REGION_GRID_GEOMETRY_TYPE>
csize_t merge_serial(T1& field, std::vector<merge_serial_uf_t>& local_csizes, RegionGrid<REGION_GRID_GEOMETRY_TYPE>& region_grid) {
	// accumulate number of total local labels
	auto num_local_labels = std::accumulate(
		local_csizes.cbegin(),
		local_csizes.cend(),
		0,
		[] (const std::size_t &a, const merge_serial_uf_t& local_csize) { return a + local_csize.size(); }
	);

	// initialize global_csize
	csize_t global_csize(2);
	global_csize.reserve(num_local_labels/5); //guess

	for (border_t border : region_grid.borders()) {
		csize_t& local_csize1 = local_csizes[border.first->index()].csize;
		csize_t& local_csize2 = local_csizes[border.second->index()].csize;

		if (border.first->nStart == border.second->nStart)
			merge_horizontal_border(*border.first, *border.second, local_csize1, local_csize2, global_csize, field);
		else
			throw;
			//merge_vertical_border(*border.first, *border.second, region_grid, global_csize, field);
	}
	return global_csize;
}


template<typename T1, typename T2, typename CSIZE_T>
void merge_horizontal_border(Region& r1, Region& r2, CSIZE_T& local_csize1, CSIZE_T& local_csize2, T1& global_csize, T2& field){
	auto topRow = r1.mEnd - 1;

	auto get_root_label = [] (store_t label, const csize_t& csize) {
		while(csize[label] < 0)
			label = - csize[label];
		return label;
	};

	// iterate along border
	for(size_t j = r1.nStart; j < r1.nEnd; ++j){
		auto& top = field(topRow, j);
		auto& bottom = field(topRow + 1, j);

		if(top != 0 && bottom != 0){
			// find local root labels
			// note: the global labels are local_csize1[top] and local_csize2[bottom] not top and bottom
			top = get_root_label(top, local_csize1);
			bottom = get_root_label(bottom, local_csize2);

			if(local_csize1[top].is_global() && local_csize2[bottom].is_global()){ //both global
				auto new_globLabel = get_root_label(local_csize1[top], global_csize);
				auto old_globLabel = get_root_label(local_csize2[bottom], global_csize);

				if(new_globLabel != old_globLabel) {
					local_csize2[bottom] = new_globLabel;
					global_csize[new_globLabel] += global_csize[old_globLabel];
					global_csize[old_globLabel] = -new_globLabel;
				}
			} else if(local_csize1[top].is_global()){ // only top global
				auto globLabel = get_root_label(local_csize1[top], global_csize);
				global_csize[globLabel] += local_csize2[bottom];
				local_csize2[bottom] = globLabel;
				local_csize2[bottom].make_global();
			} else if(local_csize2[bottom].is_global()){ // only bottom global
				auto globLabel = get_root_label(local_csize2[bottom], global_csize);
				global_csize[globLabel] += local_csize1[top];
				local_csize1[top] = globLabel;
				local_csize1[top].make_global();
			} else{ //both local
				const auto new_glob_label = global_csize.size();
				global_csize.push_back(local_csize1[top] + local_csize2[bottom]);
				local_csize1[top] = local_csize2[bottom] = new_glob_label;
				local_csize1[top].make_global();
				local_csize2[bottom].make_global();
			}
		}
	}
}

template<typename T2, typename T3>
void merge_vertical_border(Region& r1, Region& r2, T2& global_csize, const T3& field){
	throw; // not implemented
}

template<typename T1>
void write_labels_to_region(T1& field, const merge_serial_uf_t& local_csize, const Region& region, const csize_t& global_csize, const size_t global_offset){
	for(auto i = region.mStart; i < region.mEnd; ++i){
		for(auto j = region.nStart; j < region.nEnd; ++j){
			auto label = field(i,j);
			if(label == 0)
				continue;

			//find local root
			while(local_csize.csize[label] < 0)
				label = - local_csize.csize[label];

			if(local_csize.csize[label].is_global()) {
				label=local_csize.csize[label];

				//find global root
				while(global_csize[label] < 0)
					label = - global_csize[label];

				field(i, j) = label;
			}else{
				field(i,j) = label + global_offset;
			}
		}
	}
}


#endif //PROJECT_MERGE_SERIAL_HPP

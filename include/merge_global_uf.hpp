#ifndef PROJECT_MERGE_GLOBAL_UF_HPP
#define PROJECT_MERGE_GLOBAL_UF_HPP

#include "types.hpp"
#include "datastructures/global_union_find.hpp"

template<typename  T1, typename uf_global_t>
void merge_horizontal_border_uf_global(Region& r1, Region& r2, T1& field, uf_global_t& union_find_global);

template<typename  T1, typename uf_global_t>
void merge_vertical_uf_global(Region& r1, Region& r2, T1& field, uf_global_t& union_find_global);

template<typename T1, typename uf_global_t>
void merge_global_uf(T1& field, uf_global_t& union_find_global) {
	auto& region_grid = union_find_global.get_region_grid_ref();

	for (border_t border : region_grid.borders()) {
		if (border.first->nStart == border.second->nStart)
			merge_horizontal_border_uf_global(*border.first, *border.second, field, union_find_global);
		else
			merge_vertical_uf_global(*border.first, *border.second, field, union_find_global);
	}
}

template<typename  T1, typename uf_global_t>
void merge_horizontal_border_uf_global(Region& r1, Region& r2, T1& field, uf_global_t& union_find_global) {
	auto topRow = r1.mEnd - 1;
	for(size_t j = r1.nStart; j < r1.nEnd; ++j) {
		auto top = field(topRow, j);
		auto bottom = field(topRow + 1, j);
		if(top != 0 && bottom != 0) {
			union_find_global.merge(top, r1.index(), bottom, r2.index() );
		}
	}
}

template<typename  T1, typename uf_global_t>
void merge_vertical_uf_global(Region& r1, Region& r2, T1& field, uf_global_t& union_find_global) {
	auto leftCol = r1.nEnd - 1;
	//go along border
	for(size_t j = r1.mStart; j < r1.mEnd; ++j) {
		auto top = field(j, leftCol);
		auto bottom = field(j, leftCol + 1);
		if(top != 0 && bottom != 0){
			union_find_global.merge(top, r1.index(), bottom, r2.index());
		}
	}
}

template<typename  T1, typename uf_global_t>
void write_labels_to_region_global_uf(T1& field, uf_global_t& union_find_global, size_t region_label) {
	const auto& region = union_find_global.get_region_grid_ref().regions[region_label];
	for(size_t i = region.mStart; i < region.mEnd; ++i) {
		for(size_t j = region.nStart; j < region.nEnd; ++j) {
			size_t label = field(i,j);
			field(i,j) = union_find_global.find(label, region.index());
		}
	}
}

#endif //PROJECT_MERGE_GLOBAL_UF_HPP

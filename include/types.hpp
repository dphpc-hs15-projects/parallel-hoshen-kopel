#ifndef TYPES_HPP
#define TYPES_HPP

#include <cstdint>
#include <cstddef>
#include <vector>
#include <unordered_map>
#include "datastructures/csize_node.hpp"

typedef double calc_t;
typedef std::size_t size_t;

//match types to field size
typedef int64_t store_t;
typedef store_t label_t;

typedef std::vector<CSizeNode<store_t>> csize_t;
typedef std::unordered_map<store_t,size_t> cdistrib_t;

enum Geometry : uint8_t {tiles = 1, stripes = 2};

enum NEIGHBOUR : uint8_t {TOP=0, BOTTOM=1, LEFT=2, RIGHT=3};
#endif
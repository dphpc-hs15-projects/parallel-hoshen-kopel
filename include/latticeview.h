#include <iostream>
#include <fstream>
#include <vector>

#ifndef LATTICEVIEW_HEADER
#define LATTICEVIEW_HEADER
//using namespace std;

#ifndef IMAGEMAGICK
#define IMAGEMAGICK false
#define LATTICE_DEFAULT_OUTPUT_EXT "ppm"
#else
#define LATTICE_DEFAULT_OUTPUT_EXT "png"
#endif

template<typename T>
void print_latticeV2 (T const& vlat, const int &vheight, const int &vwidth, const std::string filename="output.ppm", const int& offset_x=0, const int& offset_y=0)
{
  // validate filename
  std::string filename_ext = filename.substr(filename.find_last_of(".") + 1);
  std::string filename_basename = filename.substr(0, filename.find_last_of("."));

  if (!(filename_ext == "ppm" || filename_ext == "png") || filename.find_last_of(".") == std::string::npos)
    throw std::invalid_argument("invalid file extension");
  if (!IMAGEMAGICK && filename_ext == "png")
    throw std::invalid_argument("png file extension not supported. compile with imagemagick support");

  // write to file
  std::ofstream out(filename_basename+".ppm");

  out << "P3" << std::endl;
  out << vwidth << " " << vheight << std::endl;
  out << "255" << std::endl;

  int  i, j;
  for (i=0; i<offset_x+vheight; ++i) {
    for (j=0; j<offset_y+vwidth; ++j) {
          const auto& val = vlat(i, j);
          out << (val!=0)*(21*val+17)%256 << " " << (val!=0)*(37*val+73)%256 << " " << (val!=0)*(47*val+13)%256 << " ";
    }
  }
  out << std::endl;

  out.close ();

  //convert to png
  if (filename_ext == "png") {
    //call imagemagick
    std::ostringstream cmd;
    cmd << IMAGEMAGICK << ' ' << filename_basename.c_str() << ".ppm " << filename_basename.c_str() << ".png";
    int exit_code = std::system(cmd.str().c_str());
    if (exit_code)
      throw std::runtime_error(std::string("convert to png failed with error code ") + std::to_string(exit_code));

    //delete ppm
    std::remove((filename_basename+".ppm").c_str());
  }
}

/*template<typename T>
void print_lattice (T vlat, const int &vlx, const int &vly, const int &vwidth, const int &vheight, const char* vfilename="output.ppm")
{
  int  i, j, k, l;
  int vw= vwidth/vlx, vh=vheight/vly;
  int r[5], g[5], b[5];

  r[0]= 255; g[0]= 255; b[0]= 255; //white
  r[1]=   0; g[1]= 255; b[1]=   0; //green
  r[2]= 255; g[2]=   0; b[2]=   0; //red
  r[3]=   0; g[3]=   0; b[3]=   0; //black
  r[4]=   0; g[4]=   0; b[4]= 255; //blue

  std::ofstream out (vfilename);

  out << "P3" << std::endl;
  out << vw*vlx << " " << vh*vly << std::endl;
  out << "255" << std::endl;

  for (i=vly-1; i>=0; i--)
    for (j=0; j<vh; j++)
      for (k=0; k<vlx; k++)
      {
        for (l=0; l<vw; l++)
        { out << r[vlat[k+i*vlx]%5] << " " << g[vlat[k+i*vlx]%5] << " " << b[vlat[k+i*vlx]%5] << " ";
        }
      } 
      out << std::endl;

  out.close ();
}*/

template<typename T>
void print_lattice_stdout (T const& field) {
  for(size_t i=0; i<field.m; ++i){
    for(size_t j=0; j<field.n; ++j) {
      std::cout << std::setw(5) << field(i, j);
    }
    std::cout << std::endl;
  }
}

//template <typename T>
//void print_lattice (T const& field){

#endif
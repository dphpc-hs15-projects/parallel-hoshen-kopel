#ifndef HOSHEN_PTHREADS_HPP
#define HOSHEN_PTHREADS_HPP

#include <thread>
#include <future>
#include <list>

#include "types.hpp"
#include "hoshen_naive.hpp"
#include "hoshen_uf.hpp"
#include "hoshen_fsm.hpp"

#include "datastructures/region.hpp"
#include "datastructures/region_grid.hpp"
#include "datastructures/union_find.hpp"
#include "merge_serial.hpp"

#include "profiler/profiler.hpp"
#include "profiler/rdtsc_timer.hpp"

template<typename T1, typename T2, typename KERNEL_FUNC>
void hoshen_kopel_pthreads(const T1& field, T2& output,  size_t nThreads, KERNEL_FUNC kernel){
	typedef UnionFind<store_t, CSizeNode<store_t>> uf_t; // arg...
	typedef uf_t::csize_t csize_t;

	const size_t n = field.n;
	const size_t m = field.m;

	assert(output.m == field.m);
	assert(output.n == field.n);
	
	const Geometry geometry = Geometry::stripes;
	RegionGrid<geometry> region_grid(m, n, nThreads);

	// run kernel on each region
	std::list<std::future<uf_t>> local_csize_futures;
	for (Region& region : region_grid.regions) {
		assert(local_csize_futures.size() == region.index());
		local_csize_futures.emplace_back(std::async(
			std::launch::async,
			kernel, std::ref(field), std::ref(output), std::ref(region)
		));
	}

	// join threads
	std::vector<uf_t> local_csizes;
	local_csizes.reserve(local_csize_futures.size());
	for (std::future<uf_t>& local_csize_future : local_csize_futures) {
		local_csizes.push_back(local_csize_future.get());
	}

	// merge serial
	auto global_csize = merge_serial(output, local_csizes, region_grid);

	//set offset labels
	std::vector<size_t> offsets;
	size_t total_offset = 0;
	for (uf_t& local_csize : local_csizes) {
		offsets.push_back(total_offset);
		total_offset+= local_csize.size();
	}

	std::list<std::thread> threads;
	for (Region& region : region_grid.regions) {
		//write_labels_to_region<T2>(std::ref(output), std::ref(region), std::ref(global_csize));
		threads.push_back( std::thread(
			write_labels_to_region<T2>, 
			std::ref(output), std::ref(local_csizes[region.index()]), std::ref(region), std::ref(global_csize), offsets[threads.size()-1]
		));
	}

	for (std::thread& t : threads) {
		t.join();
	}
}

template<typename T1, typename T2>
void hoshen_kopel_pthreads(const T1& field, T2& output, size_t nThreads) {
	typedef UnionFind<store_t, CSizeNode<store_t>> uf_t; // arg...
	typedef uf_t::label_t label_t;

	auto kernel = [] (const T1& field, T2& output, Region& r) {
		return hoshen_uf<T1, T2, label_t>(field, output, r);
	};
	return hoshen_kopel_pthreads(field, output, nThreads, kernel);
}

#endif //HOSHEN_PTHREADS_HPP

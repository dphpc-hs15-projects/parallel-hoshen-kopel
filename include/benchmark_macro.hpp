#ifndef BENCHMARK_HPP
#define BENCHMARK_HPP

#include <chrono>

// benchmark helper macro
using std::chrono::system_clock;
using std::chrono::duration;
#define BENCHMARK(expr)													\
[&] (std::size_t num_ops, std::string desc = "") {						\
	/* tic */															\
	auto start = system_clock::now();									\
																		\
	for (std::size_t BENCH_IDX=0; BENCH_IDX < num_ops; BENCH_IDX++) {	\
		decltype(expr) result = expr;									\
		escape(result);													\
	}																	\
																		\
	/* toc */															\
	auto end = system_clock::now();										\
																		\
	auto d = duration<double>(end-start).count();						\
	std::cout << "benchmark expr: " << #expr 							\
			  << " " << d;												\
			  															\
			  															\
	if (desc != "")														\
		std::cout << " desc: " << desc << std::endl;					\
	else																\
		std::cout << std::endl;											\
	return std::make_tuple(												\
		duration<double>(end-start).count(),							\
		std::string(#expr),												\
		desc															\
	);																	\
}
#endif
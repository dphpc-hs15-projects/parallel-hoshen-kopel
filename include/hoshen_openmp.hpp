#ifndef HOSHEN_OPENMP_HPP
#define HOSHEN_OPENMP_HPP

//#include <omp.h>
#include <vector>


template<typename T>
void hoshen_partial(T& field, size_t nStart, size_t nEnd, size_t mStart, size_t mEnd);

template<typename T>
int hoshen_kopel_openmp(T& field, size_t setThreads){

	assert(field.size() != 0);

	const size_t n = field.size();
	const size_t m = field[0].size();

	//auto nThreads = omp_get_num_procs();


	#pragma omp parallel num_threads(setThreads)
	{
		auto nThreads = omp_get_num_threads();
		auto idThread = omp_get_thread_num();
		size_t lines = n / nThreads;
		if(idThread == nThreads - 1){
			hoshen_partial(field, idThread*lines, (idThread+1)*lines + (n % nThreads) , 0, m);
		}else{
			hoshen_partial(field, idThread*lines, (idThread+1)*lines  , 0, m);
		}
	}

	return field.size(); // for benchmarking

}

template<typename T>
void hoshen_partial(T& field, size_t nStart, size_t nEnd, size_t mStart, size_t mEnd){
	assert(field.size() != 0);
	assert(nStart <= nEnd);
	assert(mStart <= mEnd);
	assert(nEnd <= field.size());
	assert(mEnd <= field[0].size());

	const size_t n = nEnd - nStart;
	const size_t m = mEnd - mStart;

	/*
	#pragma omp critical
	{
		std::cout << "id: " << omp_get_thread_num() << " parameters" << nStart << " " << nEnd << " " << mStart << " " << mEnd << std::endl;
	}
	 */

	size_t k = 2;
	std::vector<sint_t > M(n*m + 3);

	store_t top,left,current;
	for(size_t i=nStart; i<nEnd; ++i){
		for(size_t j=mStart; j<mEnd; ++j){
			current = field[i][j];

			//continue if field is unoccupied
			if(current == 0)
				continue;

			//get top and left label
			top = i < nStart+1 ? 0 : field[i-1][j];
			left = j < mStart+1 ? 0 : field[i][j-1];

			//check labels for connected cluster
			while(M[top] < 0)
				top = - M[top];
			while(M[left] < 0)
				left = - M[left];

			//set current label
			if(top == 0 && left == 0){
				field[i][j] = k;
				M[k] = 1;
				++k;
			}else if(top != 0 && left == 0){
				field[i][j] = top;
				M[top] += 1;
			}else if(top == 0 ){ //left always != 0
				field[i][j] = left;
				M[left] += 1;
			}else if(top == left){
				field[i][j] = left;
				M[left] += 1;
			}else{
				field[i][j] = top;
				M[top] += M[left]+1;
				M[left] = -top;
			}

		}
	}


	//unify cluster labels
	sint_t label;
	for(size_t i=nStart; i<nEnd; ++i){
		for(size_t j=mStart; j<mEnd; ++j){
			label = field[i][j];
			while(M[label] < 0)
				label = - M[label];
			field[i][j] = label;
		}
	}
}

#endif
#ifndef HOSHEN_UF_PTHREADS
#define HOSHEN_UF_PTHREADS

#include <thread>
#include <future>
#include <list>

#include "types.hpp"

#include "datastructures/region.hpp"
#include "datastructures/region_grid.hpp"
#include "datastructures/global_union_find_thread_safe.hpp"

#include "hoshen_uf.hpp"
#include "hoshen_fsm.hpp"

#include "merge_global_uf.hpp"
#include "hierarchical_merge.hpp"

template<typename MERGE_DATASTRUCTURE, typename T1, typename T2, typename MERGE_KERNEL>
MERGE_DATASTRUCTURE hoshen_kopel_uf_pthreads_impl(const T1& field, T2& output, size_t nThreads, MERGE_KERNEL kernel) {
	typedef typename MERGE_DATASTRUCTURE::uf_t uf_t; // arg...
	/*auto kernel = [] (const T1& field, T2& output, Region& r) {
		hoshen_kopel_uf(field, output, r);
	};
	return hoshen_kopel_pthreads(field, output,nThreads, kernel);*/
	const size_t n = field.n;
	const size_t m = field.m;

	assert(output.m == field.m);
	assert(output.n == field.n);
	assert(nThreads != 0);

	constexpr Geometry geometry = MERGE_DATASTRUCTURE::geometry;
	RegionGrid<geometry> region_grid(m, n, nThreads);

	std::list<std::future<uf_t>> union_find_futures;

	for (Region& region : region_grid.regions) {
		union_find_futures.emplace_back(
			std::async(
				std::launch::async,
				static_cast<uf_t(*)(const T1&, T2&, const Region&)>(hoshen_uf<T1, T2, CSizeNode<typename T2::value_type>>),
				std::ref(field),
				std::ref(output),
				std::ref(region)
			)
		);
	}

	std::vector<uf_t> union_finds;
	union_finds.reserve(region_grid.regions.size());
	for (std::future<uf_t>& uf_future : union_find_futures) {
		union_finds.push_back(uf_future.get());
	}

	MERGE_DATASTRUCTURE uf_global(std::move(region_grid), std::move(union_finds));

	//merge_global_uf(output, uf_global);
	kernel(output, uf_global);
	uf_global.resolve_circles(); // important if using UnionFindGlobal_ThreadSafe !!!!
	uf_global.calculate_sizes(); //optional for UnionFindGlobal_ThreadSafe

	//write labels to field
	std::list<std::thread> threads;
	for(const Region& region : uf_global.get_region_grid_ref().regions) {
		//write_labels_to_region_global_uf(output, uf_global, region, ufinds);
		threads.emplace_back(write_labels_to_region_global_uf<T2, decltype(uf_global)>, std::ref(output), std::ref(uf_global), region.index());
	}
	for (std::thread& t : threads) {
		t.join();
	}
	return uf_global;
}

enum Merge_Type : int {serial, parallel};

template<Geometry geom, std::underlying_type<Merge_Type>::type n, typename OUTPUT_T>
struct merge_deduction{
};

template<Geometry geom, typename OUTPUT_T>
struct merge_deduction<geom, Merge_Type::serial, OUTPUT_T>{
	typedef UnionFindGlobal<geom> type;
	static constexpr auto kernel = merge_global_uf<OUTPUT_T , type>;
};

template<Geometry geom, typename OUTPUT_T>
struct merge_deduction<geom, Merge_Type::parallel, OUTPUT_T>{
	typedef UnionFindGlobal_ThreadSafe<geom> type;
	static constexpr auto kernel = hierarchical_merge<OUTPUT_T , type>;
};


template<Geometry geometry, Merge_Type merging, typename T1, typename T2>
typename merge_deduction<geometry, merging, T2>::type hoshen_kopel_uf_pthreads(const T1& field, T2& output, size_t nThreads){
	return 	hoshen_kopel_uf_pthreads_impl<typename merge_deduction<geometry, merging, T2>::type>(field, output, nThreads, merge_deduction<geometry, merging, T2>::kernel);
}
#endif
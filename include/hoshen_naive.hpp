#ifndef HOSHEN_NAIVE_HPP
#define HOSHEN_NAIVE_HPP

#include <cassert>
#include <memory>
#include "datastructures/region.hpp"
#include "datastructures/union_find.hpp"

template<typename T1, typename T2, typename LABEL_T = typename T2::value_type>
UnionFind<typename T2::value_type, LABEL_T> hoshen_naive(const T1& field, T2& output) {
	Region region(0, field.m, 0, field.n);
	auto result = hoshen_naive<T1, T2, LABEL_T>(field, output, region);
	auto& csize = result.csize;

	// unify cluster labels
	store_t label;
	for(size_t i=region.mStart; i<region.mEnd; ++i){
		for(size_t j=region.nStart; j<region.nEnd; ++j){
			label = output(i, j);
			while(csize[label] < 0)
				label = - csize[label];
			output(i, j) = label;
		}
	}

	return result;
}

/**
 * [nStart, nEnd[ x [mStart, nEnd[
 */
template<typename T1, typename T2, typename LABEL_T = typename T2::value_type>
UnionFind<typename T2::value_type, LABEL_T> hoshen_naive(const T1& field, T2& output, const Region& region) {
	typedef UnionFind<typename T2::value_type, LABEL_T> uf_t;

	assert(field.m != 0 && field.n != 0);
	assert(region.mEnd <= field.m);
	assert(region.nEnd <= field.n);

	const size_t& m = region.rows();
	const size_t& n = region.cols();

	typename uf_t::csize_t csize(2);
	csize.reserve(n*m/2 + 3);

	store_t top,left;
	bool current;
	for(size_t i=region.mStart; i<region.mEnd; ++i){
		for(size_t j=region.nStart; j<region.nEnd; ++j){
			current = field(i, j);

			//continue if field is unoccupied
			if(current == 0)
				continue;

			//get top and left label
			top = (i < region.mStart+1) ? 0 : output(i-1, j);
			left = (j < region.nStart+1) ? 0 : output(i, j-1);

			//check labels for connected cluster
			while(csize[top] < 0)
				top = - csize[top];
			while(csize[left] < 0)
				left = - csize[left];

			//set current label
			if(top == 0 && left == 0) {
				output(i, j) = csize.size();
				csize.push_back(1);
			} else if(top != 0 && left == 0) {
				//output(i-1, j) = top;
				output(i, j) = top;
				csize[top] += 1;
			} else if(top == 0) { //left always != 0
				//output(i, j-1) = left;
				output(i, j) = left;
				csize[left] += 1;
			} else if(top == left) {
				output(i, j) = left;
				csize[left] += 1;
			} else {
				output(i, j) = top;
				csize[top] += csize[left]+1;
				csize[left] = -top;
			}
		}
	}

	return uf_t(output, std::move(csize));
}

#endif
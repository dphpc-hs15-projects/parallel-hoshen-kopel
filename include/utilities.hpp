#ifndef UTIL_HPP
#define UTIL_HPP

#include <random>
#include <iostream>
#include <fstream>

template <typename T>
static void escape (T&& p) {
    asm volatile("" :  : "g"(p));
}

template<typename T>
void create_perc(T& field, calc_t const& p, const int seed=42){
	std::mt19937_64 rand_gen(seed);
	std::uniform_real_distribution<calc_t> rand_num(0,1);

	for(size_t i=0; i<field.m; ++i){
		for(size_t j=0; j<field.n; ++j) {
			field(i, j) = rand_num(rand_gen) < p;
		}
	}

}

template<typename T>
void create_horizontal_perc(T& field){

	size_t num_stripes = field.m / 5;

	static std::mt19937_64 rand_gen(42);
	static std::uniform_int_distribution<size_t> rand_row(0,field.m-1);
	static std::uniform_int_distribution<short> horizontal_displace(-1,1);
	static std::uniform_int_distribution<short> vertical_displace(-2,2);

	for(size_t i=0; i<field.m; ++i){
		for(size_t j=0; j<field.n; ++j) {
			field(i, j) = 0;
		}
	}

	size_t row, col;

	for (size_t i = 0; i < num_stripes; ++i) {
		row = rand_row(rand_gen);
		for (int j = 0; j < field.n; ++j) {
			do {
				row += vertical_displace(rand_gen);
			} while (row < 0 || row >= field.m);
			do {
				col = j + horizontal_displace(rand_gen);
			} while (col < 0 || col >= field.n);
			field(row,col) = 1;
		}
	}

}



template<typename T>
void create_vertical_perc(T& field){

	size_t num_stripes = field.n / 2;

	static std::mt19937_64 rand_gen(42);
	static std::uniform_int_distribution<size_t> rand_col(0,field.n-1);
	static std::uniform_int_distribution<short> horizontal_displace(-1,1);
	static std::uniform_int_distribution<short> vertical_displace(-1,1);

	for(size_t i=0; i<field.m; ++i){
		for(size_t j=0; j<field.n; ++j) {
			field(i, j) = 0;
		}
	}

	size_t row, col;

	for (size_t i = 0; i < num_stripes; ++i) {
		col = rand_col(rand_gen);
		for (int j = 0; j < field.m; ++j) {
			do {
				col += horizontal_displace(rand_gen);
			} while (col < 0 || col >= field.n);
			do {
				row = j + vertical_displace(rand_gen);
			} while (row < 0 || row >= field.m);
			field(row,col) = 1;
		}
	}

}



template <typename T>
void create_ball_perc(T& field){

	size_t num_balls = field.n/10;
	int radius = (int) field.n /10;

	static std::mt19937_64 rand_gen(42);
	static std::uniform_int_distribution<size_t> rand_row(0,field.m-1);
	static std::uniform_int_distribution<size_t> rand_col(0,field.n-1);
	static std::uniform_int_distribution<int> rand_displace(-radius,radius);
	static std::uniform_real_distribution<calc_t> rand_num(0,1);



	for(size_t i=0; i<field.m; ++i){
		for(size_t j=0; j<field.n; ++j) {
			field(i, j) = rand_num(rand_gen) < 0.1;
		}
	}

	size_t cx, cy, row, col;

	for (size_t i = 0; i < num_balls; ++i) {
		cx = rand_row(rand_gen);
		cy = rand_col(rand_gen);
		for (int c = 0; c < radius*radius; ++c) {
			row = rand_displace(rand_gen);
			col = rand_displace(rand_gen);
			if (cx + row >= 0 && cx + row < field.m && cy + col >= 0 && cy + col < field.n) {
				if (row*row + col*col < radius*radius && rand_num(rand_gen) < 0.9)
 					field(cx+row,cy+col) = 1;
				else if (rand_num(rand_gen) < 0.1)
					field(cx+row,cy+col) = 1;
			}
		}

	}
}





/*
prints the list to the command line
*/
template <typename T>
void print_list (T& list) {

	for (auto& element : list)
		std::cout << element << " ";
}

template <typename T>
cdistrib_t compute_cluster_size_distribution (T& list) {

	cdistrib_t cdistrib;
	for (auto const& element : list) {
		if (element > 0) {
			auto pos = cdistrib.find(element);
			if (pos == cdistrib.end())
				cdistrib.emplace(element,1);
			else
				pos->second += 1;
		}
	}
	return cdistrib;
}

template <typename T>
void write_cluster_size_distribution (T& list, size_t m, size_t n, calc_t p) {

	cdistrib_t cdistrib;
	cdistrib = compute_cluster_size_distribution(list);
	std::ofstream output;
	std::string filepath = "../docs/";
	std::string filename = "cluster_distribution_"+std::to_string(m)+"x"+std::to_string(n)+"_p="+std::to_string(p)+".dat";

	output.open(filepath+filename);
	output << "size of cluster \t number of clusters" << std::endl;
	for (auto& element : cdistrib)
		output << element.first << "\t" << element.second << std::endl;
	output.close();
}

template <typename T>
void calculate_csize_sparsity (T& list, calc_t& ratio, calc_t& average_redirections) {

	ratio = 0.0;
	average_redirections = 0.0;
	size_t size = list.size();
	size_t pos;
	for (size_t i = 2; i < size; ++i) {
		pos = i;
		if (list[pos] > 0) {
			++ratio;
			++average_redirections;
		} else {
			while(list[pos] < 0) {
				pos = - list[pos];
				++average_redirections;
			}
		}
	}
	//first two values of csize are always 0 and are therefore not counted
	ratio /= (size-2);
	average_redirections /= (size-2);
}


#endif
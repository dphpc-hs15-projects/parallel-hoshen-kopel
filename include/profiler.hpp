#ifndef PROFILER_HPP
#define PROFILER_HPP

#include "profiler/rdtsc_timer.hpp"

// boost accumulators
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>

// boost property tree
#include <boost/property_tree/ptree.hpp>

#include <thread>         // std::this_thread::sleep_for
#include <chrono>
#include <unordered_map>
#include <string>
#include <cmath>

#include <iomanip>
#include <utility>
#include <vector>

using boost::property_tree::ptree;
using namespace boost::accumulators;

template <typename ARGUMENT_T = void*>
struct BenchmarkState {
	typedef ARGUMENT_T argument_t;

	// timer
	util::rdtsc_timer timer;

	// attributes
	std::unordered_map<std::string, std::string> attrs;

	// arguments
	argument_t argument;

	size_t iterations=0;

	BenchmarkState(ARGUMENT_T argument_) : argument(argument_) {}

	BenchmarkState() {}

	// accumulators
	accumulator_set<long double, stats<tag::sum, tag::min, tag::max, tag::mean, tag::variance(lazy)> > time_accumulator;
	accumulator_set<long double, stats<tag::sum, tag::min, tag::max, tag::mean> > cycle_accumulator;

	// return true until a predefined number of seconds has elapsed
	bool keep_running() {
		return sum(time_accumulator) < 60 && iterations < 100000u;
	}

	void start() {
		iterations++;
	}

	void stop() {
		time_accumulator(timer.sec());
		cycle_accumulator(timer.cycles());
	}

	template <typename T>
	void set_attr(const std::string& val, const T& attr) {
		std::stringstream ss;
		ss << attr;
		attrs.emplace(val, ss.str());
	}

	ptree properties() {
		ptree pt;

		pt.put("iterations", iterations);

		// time
		ptree time_pt;
		time_pt.put<double>("min", min(time_accumulator));
		time_pt.put<double>("max", max(time_accumulator));
		time_pt.put<double>("mean", mean(time_accumulator));
		time_pt.put<double>("variance", variance(time_accumulator));
		time_pt.put<double>("stddev", std::sqrt(variance(time_accumulator)));
		pt.put_child("time", time_pt);

		// cycles
		ptree cycles_pt;
		cycles_pt.put<double>("min", min(cycle_accumulator));
		cycles_pt.put<double>("max", max(cycle_accumulator));
		cycles_pt.put<double>("mean", mean(cycle_accumulator));
		pt.put_child("cycles", cycles_pt);

		// attributes
		ptree attrs_pt;
		for (auto& attr : attrs) {
			attrs_pt.put(attr.first, attr.second);
		}

		if (attrs.size() > 0)
			pt.put_child("attrs", attrs_pt);

		return pt;
	}

	static std::string header() {
		std::stringstream header;

		auto additional_attr_headers = argument_t::header_attributes();
		for (std::string& attr : additional_attr_headers) {
			header << std::setw(15) << attr << " ";
		}

		header << std::setw(12) << "iterations" << " "
			   << std::setw(12) << "mean" << " "
			   << std::setw(12) << "min"  << " "
			   << std::setw(12) << "max";
		return header.str();
	}
};

template <typename T>
std::ostream& operator<<(std::ostream& stream, BenchmarkState<T> state) {
	auto additional_attr_headers = BenchmarkState<T>::argument_t::header_attributes();
	for (std::string& attr : additional_attr_headers) {
		stream << std::setw(15) << state.attrs[attr] << " ";
	}

	stream << std::setw(12) << state.iterations << " ";

	if (mean(state.time_accumulator) < 0.001)
		stream << std::scientific;
	else
		stream << std::fixed;

	stream
		<< std::setw(12) << mean(state.time_accumulator) << " "
		<< std::setw(12) << min(state.time_accumulator) << " "
		<< std::setw(12) << max(state.time_accumulator)
		/*<< std::defaultfloat*/;

	stream.unsetf(std::ios_base::floatfield); // fix for gcc-4.9 instead of using std::defaultfloat

	if (mean(state.time_accumulator) < 0.001)
		stream << std::scientific;

	return stream;
}

struct Profiler {
	BenchmarkState<> benchmark(void(*benchmark_func)(BenchmarkState<>&)) {
		BenchmarkState<> state;
		benchmark_func(state);
		return state;
	}

	template <typename ARGUMENT_T, typename BENCHMARK_FUNC>
	BenchmarkState<ARGUMENT_T> benchmark(ARGUMENT_T arg, BENCHMARK_FUNC benchmark_func) {
		BenchmarkState<ARGUMENT_T> state(arg);
		benchmark_func(state);
		return state;
	}
};

#define BENCH_TIMER_START(state)	\
	state.start(); 					\
	state.timer.start();

#define BENCH_TIMER_STOP(state)		\
	state.timer.stop();				\
	state.stop();

#endif
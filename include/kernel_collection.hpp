#ifndef KERNEL_COLLECTION_HPP
#define KERNEL_COLLECTION_HPP

#include <string>
#include <vector>
#include <utility>

#include "datastructures/union_find.hpp"
#include "hoshen_naive.hpp"
#include "hoshen_uf.hpp"
#include "hoshen_fsm.hpp"

/**
 * \struct kernel_collection
 * \brief functor that returns a vector of function pointers to all kernels
 */
template <typename T1, typename T2, typename LABEL_T>
struct kernel_collection {
	using union_find_t = UnionFind<typename T2::value_type, LABEL_T>;

	using kernel_func_ptr_t = union_find_t(*)(const T1&, T2&);

	using kernel_collection_t = std::unordered_map<std::string, kernel_func_ptr_t>;

	kernel_collection_t operator()() {
		kernel_collection_t kernels;
		kernels.emplace("hoshen_naive", static_cast<kernel_func_ptr_t>(&hoshen_naive<T1, T2, LABEL_T>));
		kernels.emplace("hoshen_uf", static_cast<kernel_func_ptr_t>(&hoshen_uf<T1, T2, LABEL_T>));
		kernels.emplace("hoshen_fsm", static_cast<kernel_func_ptr_t>(&hoshen_fsm<T1, T2, LABEL_T>));
		return kernels;
	}
};

/*template <typename T1, typename T2, typename LABEL_T>
struct parallel_implementations {
	using kernel_collection_t = std::unordered_map<std::string, kernel_func_ptr_t>;

	kernel_collection_t operator()() {
		kernel_collection_t kernels;
		kernels.emplace("hoshen_naive", static_cast<kernel_func_ptr_t>(&hoshen_naive<T1, T2, LABEL_T>));
		kernels.emplace("hoshen_uf", static_cast<kernel_func_ptr_t>(&hoshen_uf<T1, T2, LABEL_T>));
		kernels.emplace("hoshen_fsm", static_cast<kernel_func_ptr_t>(&hoshen_fsm<T1, T2, LABEL_T>));
		return kernels;
	}
};*/
#endif
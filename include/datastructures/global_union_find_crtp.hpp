#ifndef PROJECT_GLOBAL_UNION_FIND_CRTP_HPP
#define PROJECT_GLOBAL_UNION_FIND_CRTP_HPP

#include <vector>

#include "types.hpp"
#include "datastructures/union_find.hpp"
#include "datastructures/region.hpp"
#include "datastructures/region_grid.hpp"

/*******************
 * UnionFindGLobal CRTP pattern
 *******************/
template<Geometry GEOMETRY, class DERIVED_T>
class UnionFindGlobal_CRTP {
public:
	static constexpr Geometry geometry = GEOMETRY;
	typedef DERIVED_T derived_t;
	typedef store_t glob_size_t;
	typedef store_t glob_label_t; //may have to be adjusted
	typedef UnionFind<store_t, CSizeNode<store_t>> uf_t;
	//typedef std::vector<std::pair<Region, UnionFind> > local_cont_t;

	std::vector<size_t> offsets;
	std::vector<uf_t> union_finds;

	UnionFindGlobal_CRTP(const UnionFindGlobal_CRTP&) = delete;
	UnionFindGlobal_CRTP(UnionFindGlobal_CRTP&&) = default;

	UnionFindGlobal_CRTP(RegionGrid<geometry>&& region_grid_, std::vector<uf_t>&& union_finds_) :
			region_grid(std::move(region_grid_)), union_finds(std::move(union_finds_)), total_offset(0){
		//set offset labels
		total_offset = 0;
		for (uf_t& union_find : union_finds) {
			offsets.push_back(total_offset);
			total_offset+= union_find.size();
		}
		//glob_sizes.reserve(total_region_offset); //guess amount of global labels
	}

	inline glob_label_t find(store_t label, size_t region_idx) {
		return static_cast<derived_t*>(this) -> find(label, region_idx);
	}

	inline RegionGrid<geometry>& get_region_grid_ref() { return region_grid; }

	inline glob_size_t get_cluster_size(store_t local_label, const size_t& region_idx){
		return static_cast<derived_t*>(this) -> get_cluster_size(local_label, region_idx);
	}
	inline glob_size_t get_cluster_size(const glob_label_t glob_label){
		return static_cast<derived_t*>(this) -> get_cluster_size(glob_label);
	}

	inline void merge(store_t label1, size_t region_idx1, store_t label2, size_t region_idx2);

	inline void resolve_circles() {}; //optional implementation
	inline void calculate_sizes() {}; //optional implementation

protected:
	inline void merge_both_local(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2){
		static_cast<derived_t*>(this) -> merge_both_local(label1, region_idx1, label2, region_idx2);
	}
	inline void merge_both_global(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2){
		static_cast<derived_t*>(this) -> merge_both_global(label1, region_idx1, label2, region_idx2);
	}
	inline void merge_global_and_local(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2){
		static_cast<derived_t*>(this) -> merge_global_and_local(label1, region_idx1, label2, region_idx2);
	}

	RegionGrid<geometry> region_grid;

	size_t total_offset; //sum of all offsets of all regions

	friend derived_t;
};

/************************************************
 * Implementation
 ************************************************/
template<Geometry geometry, class derived_t>
void UnionFindGlobal_CRTP<geometry, derived_t>::merge(store_t label1, size_t region_idx1, store_t label2, size_t region_idx2) {
	auto& union_find1 = union_finds[region_idx1];
	auto& union_find2 = union_finds[region_idx2];

	label1 = union_find1.find_by_label(label1);
	label2 = union_find2.find_by_label(label2);

	bool is_global1 = union_find1.csize[label1].is_global();
	bool is_global2 = union_find2.csize[label2].is_global();

	if( is_global1 && is_global2){ //both global
		merge_both_global(label1, region_idx1, label2, region_idx2);
	}else if(is_global1){ //only label 1 global
		merge_global_and_local(label1, region_idx1, label2, region_idx2);

	}else if(is_global2){ //only label 2 global
		merge_global_and_local(label2, region_idx2, label1, region_idx1);

	}else{ //both local
		merge_both_local(label1, region_idx1, label2, region_idx2);
	}
}

#endif //PROJECT_GLOBAL_UNION_FIND_CRTP_HPP

#ifndef PROJECT_GLOBAL_UNION_FIND_THREAD_SAFE_HPP
#define PROJECT_GLOBAL_UNION_FIND_THREAD_SAFE_HPP


#include "types.hpp"
#include "datastructures/union_find.hpp"
#include "datastructures/region.hpp"
#include "datastructures/region_grid.hpp"
#include <vector>
#include <unordered_map>
#include <cassert>
#include <atomic>
#include <list>
#include <algorithm>
#include <boost/lockfree/queue.hpp>

#include "datastructures/global_union_find_crtp.hpp"

template<Geometry GEOMETRY>
class UnionFindGlobal_ThreadSafe : public UnionFindGlobal_CRTP<GEOMETRY, UnionFindGlobal_ThreadSafe<GEOMETRY>>{
public:
	static constexpr Geometry geometry = GEOMETRY;

	typedef UnionFindGlobal_CRTP<geometry, UnionFindGlobal_ThreadSafe<geometry>> super;
	typedef typename super::glob_size_t glob_size_t;
	typedef typename super::glob_label_t glob_label_t;
	typedef typename super::uf_t uf_t;

	friend UnionFindGlobal_CRTP<geometry, UnionFindGlobal_ThreadSafe<geometry>>;

	UnionFindGlobal_ThreadSafe(RegionGrid<geometry> && reg_grid, std::vector<uf_t> && u_finds):
			super(std::move(reg_grid), std::move(u_finds)), current_max_label(2),
			glob_labels(static_cast<super*>(this) -> region_grid.regions.size()),
			glob_sizes_ptr(new std::vector<std::atomic<glob_size_t> >(static_cast<super*>(this) -> total_offset)),
			glob_sizes(*glob_sizes_ptr), circle_labels(0){
		// note: avoid relocation of vector
	}

	UnionFindGlobal_ThreadSafe(UnionFindGlobal_ThreadSafe&& uf_global):
			super(std::move(uf_global)), current_max_label(static_cast<glob_size_t >(uf_global.current_max_label)),
			glob_labels(std::move(uf_global.glob_labels)), glob_sizes_ptr(std::move(uf_global.glob_sizes_ptr)),
			glob_sizes(*glob_sizes_ptr), circle_labels(0){ // note: resolve circles must be called before moving
	}

	UnionFindGlobal_ThreadSafe(const UnionFindGlobal_ThreadSafe&) = delete;

	inline glob_label_t find(store_t label,size_t region_idx);
	inline void resolve_circles();
	void calculate_sizes();

	glob_size_t get_cluster_size(store_t local_label, const size_t& region_idx);
	glob_size_t get_cluster_size(glob_label_t glob_label);

	//temporary for benchmarking
	size_t size(){
		size_t size = 0;
		for(auto& uf : static_cast<super*>(this) -> union_finds){
			size += uf.csize.size() - 2;
		}
		return size + current_max_label - 2;
	}

private:
	inline void merge_both_local(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2);
	inline void merge_both_global(store_t label1, const size_t& region_idx1, store_t label2, const size_t& region_idx2);
	inline void merge_global_and_local(store_t label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2);

	inline glob_label_t find_global_root_with_circle_detection(glob_label_t label);

	inline glob_label_t find_global_root(glob_label_t label){
		while(true){
			glob_label_t current = glob_sizes[label];
			if(current >= 0)
				break;
			else
				label = - current;
		}
		return label;
	}

	glob_size_t get_size_of_global_cluster(glob_label_t glob_label);


	std::atomic<size_t> current_max_label;
	std::vector<std::list<std::pair<glob_label_t, store_t> > > glob_labels; //stores global label and size of cluster in corresponding region
	std::unique_ptr<std::vector<std::atomic<glob_size_t> >> glob_sizes_ptr;
	std::vector<std::atomic<glob_size_t> >& glob_sizes; // initialized with one zero element, stores sizes with index = global Label without total_offset
	boost::lockfree::queue<glob_size_t> circle_labels;
};

template<Geometry geometry>
typename UnionFindGlobal_ThreadSafe<geometry>::glob_label_t UnionFindGlobal_ThreadSafe<geometry>::find(store_t label,size_t region_idx) {
	if(label == 0)
		return 0;

	auto& ufind = static_cast<super*>(this) -> union_finds[region_idx];
	label = ufind.find_by_label(label);

	if(ufind.csize[label].is_global()){
		label = static_cast<super*>(this) -> union_finds[region_idx].csize[label];
		while(true) {
			glob_label_t current = glob_sizes[label];
			if (current >= 0)
				break;
			else
				label = -current;
		}

		return label + static_cast<super*>(this) -> total_offset;
	}else{
		return label + static_cast<super*>(this) -> offsets[region_idx];
	}
}

template<Geometry geometry>
void UnionFindGlobal_ThreadSafe<geometry>::merge_both_local(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2) {
	//does not use global_sizes !! -> thread safe if both regions locked
	auto& union_find1 = static_cast<super*>(this) -> union_finds[region_idx1];
	auto& union_find2 = static_cast<super*>(this) -> union_finds[region_idx2];

	//get new global label
	size_t glob_label = current_max_label++; // atomic increase

	glob_labels[region_idx1].emplace_back(glob_label, union_find1.csize[label1]);
	union_find1.csize[label1] = glob_label;

	glob_labels[region_idx2].emplace_back(glob_label, union_find2.csize[label2]);
	union_find2.csize[label2] = glob_label;

	union_find1.csize[label1].make_global();
	union_find2.csize[label2].make_global();
}

template<Geometry geometry>
void UnionFindGlobal_ThreadSafe<geometry>::merge_both_global(store_t label1, const size_t& region_idx1,store_t label2, const size_t& region_idx2) {
	//set label 2 equal to label 1, uses atomic operations in glob_sizes
	label1 = static_cast<super*>(this) -> union_finds[region_idx1].csize[label1];
	label2 = static_cast<super*>(this) -> union_finds[region_idx2].csize[label2];

	//find root label of label1 to reduce probabilty of creating circle
	label1 = find_global_root_with_circle_detection(label1);
	//label2 = find_global_root(label2);

	if(label1 == label2) return; //exit if same labels

	glob_label_t current_root = label2;
	glob_label_t desired = - label1;
	glob_label_t expected = 0;
	glob_label_t current_value;

	glob_label_t current_enry_l1 = glob_sizes[label1];
	if(-current_enry_l1 == label2)
		return; //detect direct circle

	std::list<glob_label_t> circle_list(1,current_root); //maybe change to vector
	while(true){ //atomic compare and swap
		current_value = glob_sizes[current_root];
		assert(expected == 0);
		assert(desired < 0);

		//only get exclusive write rights if current value is root
		if(current_value >= 0) {
			if (glob_sizes[current_root].compare_exchange_strong(expected, desired))
				break;
			else{
				current_root = - expected;
				expected = 0;
			}
		}else{
			current_root = -current_value;
		}

		assert(current_root >= 0);
		assert(current_root < glob_sizes.size());
		//assert(glob_sizes[current_root] != 0);
		//assert(expected < 0);
		//assert(desired < 0);

		assert(current_root > 0);

		if(current_root == label1) {
			if (glob_sizes[label1] == 0)
				break; //exit if already connected
		}

		//circle detection
		if(circle_list.end() == std::find(circle_list.begin(), circle_list.end(), -desired) && circle_list.end() == std::find(circle_list.begin(), circle_list.end(), current_root)){
			//not found
			circle_list.push_back(current_root);
		} else{
			//found
			circle_labels.push(current_root); //thread safe push
			//std::cout<< 1;
			break;
		}
		//std::cout<< std::flush;
	}
	assert(glob_sizes[current_root] <= 0);
}

template<Geometry geometry>
void UnionFindGlobal_ThreadSafe<geometry>::merge_global_and_local(store_t label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2) {
	// note: does not use global_sizes !! -> thread safe if both regions locked
	label1 = static_cast<super*>(this) -> union_finds[region_idx1].csize[label1];
	auto& union_find2 = static_cast<super*>(this) -> union_finds[region_idx2];

	glob_labels[region_idx2].emplace_back(label1, union_find2.csize[label2]);
	union_find2.csize[label2] = label1;
	union_find2.csize[label2].make_global();
}

template<Geometry geometry>
void UnionFindGlobal_ThreadSafe<geometry>::resolve_circles() {
	// note: do not run in parallel
	glob_label_t label;

	while(circle_labels.pop(label)){ // pop into label
		//label = find_global_root(label);
		std::list<glob_label_t> prev_labels(1, label);
		glob_label_t current_root = glob_sizes[label];
		if(current_root == 0)
			break;
		while(current_root < 0) {
			label = - current_root;
			if (prev_labels.end() !=std::find(prev_labels.begin(), prev_labels.end(), label)) {
				assert(glob_sizes[label] != 0);
				//found
				glob_sizes[label] = 0;
				break;
			}else{
				prev_labels.push_back(label);
				current_root = glob_sizes[label];
				if(current_root >= 0)
					break;
			}
		}
	}
}

template<Geometry geometry>
typename UnionFindGlobal_ThreadSafe<geometry>::glob_label_t UnionFindGlobal_ThreadSafe<geometry>::find_global_root_with_circle_detection(glob_label_t label) {
	std::list<glob_label_t> prev_labels;
	while(true){
		glob_label_t current = glob_sizes[label];
		if(current >= 0)
			break;
		else {
			if (prev_labels.end() !=std::find(prev_labels.begin(), prev_labels.end(), current)){
				//found circle
				circle_labels.push(current);
				return current;
			}
			prev_labels.push_back(label);
			label = -current;
		}
	}
	return label;
}

template<Geometry geometry>
void UnionFindGlobal_ThreadSafe<geometry>::calculate_sizes() {
	for(auto& list : glob_labels){
		for(auto& pair : list){
			glob_label_t label = find_global_root(pair.first);
			glob_sizes[label] += pair.second;
		}
	}
}

template<Geometry geometry>
typename UnionFindGlobal_ThreadSafe<geometry>::glob_size_t UnionFindGlobal_ThreadSafe<geometry>::get_cluster_size(store_t label, const size_t& region_idx) {
	if(label == 0)
		return 0;

	auto& ufind = static_cast<super*>(this) -> union_finds[region_idx];
	label = ufind.find_by_label(label);

	if(ufind.csize[label].is_global()){
		auto pos = glob_labels[region_idx].find(label);
		assert(pos !=  glob_labels[region_idx].end());
		return glob_sizes[find_global_root(pos->second)];
	}else{
		return static_cast<super*>(this) -> union_finds[region_idx].csize[label];
	}
}

template<Geometry geometry>
typename UnionFindGlobal_ThreadSafe<geometry>::glob_size_t UnionFindGlobal_ThreadSafe<geometry>::get_cluster_size(glob_label_t glob_label) {
	if(glob_label <= static_cast<super*>(this) -> total_offset){ //local
		//find region idx
		size_t region_idx = 0;
		for(size_t i = 1; i < static_cast<super*>(this) -> offsets.size(); ++i){
			if(glob_label < static_cast<super*>(this) -> offsets[i])
				break;
			++region_idx;
		}
		auto offset = static_cast<super*>(this) -> offsets[region_idx];
		return static_cast<super*>(this) -> union_finds[region_idx].csize[glob_label - offset];

	}else{ //global
		glob_label -= static_cast<super*>(this) -> total_offset;
		glob_label = find_global_root(glob_label);
		return glob_sizes[glob_label];
	}

}

#endif //PROJECT_GLOBAL_UNION_FIND_THREAD_SAFE_HPP

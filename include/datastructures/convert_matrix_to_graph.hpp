#ifndef PROJECT_CONVERT_MATRIX_TO_GRAPH_HPP
#define PROJECT_CONVERT_MATRIX_TO_GRAPH_HPP
#include <boost/config.hpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include "matrix.hpp"

/**
 * converts matrix to a boost graph
 * call connected components with:
 * std::vector<int> component(boost::num_vertices(graph));
 * boost::connected_components(graph, &component[0]);
 *
 */

#include <list>

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS> graph_with_vec_t;
typedef boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS> graph_with_list_t;

template<typename T, typename GRAPH_T>
GRAPH_T convert_matrix_to_graph(const Matrix<T>& matrix){
	GRAPH_T graph;

	for(size_t i = 0; i < matrix.m; ++i){
		for(size_t j = 0; j < matrix.n; ++j){
			if(matrix(i,j)){
				size_t vertex = i*matrix.n + j;

				if(i > 0)
					if(matrix(i-1,j)) {
						size_t top = (i-1) * matrix.n + j;
						boost::add_edge(vertex, top, graph);
					}

				if(j > 0)
					if(matrix(i,j-1)) {
						size_t left = i*matrix.n + j - 1;
						boost::add_edge(vertex, left, graph);
					}

			}
		}
	}

	return graph;
}

template <typename T1, typename T2, typename COMPONENTS_T>
void write_components_to_field(const Matrix<T1>& field, Matrix<T2>& output, COMPONENTS_T components){
	for(size_t i = 0; i < field.m; ++i) {
		for (size_t j = 0; j < field.n; ++j) {
			size_t index = i*field.n + j;
			if(field(i,j))
				output(i,j) = components[index];
		}
	}

}

#endif //PROJECT_CONVERT_MATRIX_TO_GRAPH_HPP

#ifndef PROJECT_GLOBAL_UNION_FIND_HPP
#define PROJECT_GLOBAL_UNION_FIND_HPP

#include "types.hpp"
#include "datastructures/union_find.hpp"
#include "datastructures/region.hpp"
#include "datastructures/region_grid.hpp"
#include <vector>
#include <unordered_map>
#include <cassert>
#include <atomic>
#include <list>
#include <algorithm>
#include <boost/lockfree/queue.hpp>

#include"datastructures/global_union_find_crtp.hpp"

//Preconditions:
//local union finds must be completed
//order of union finds in vector must correspond to region labels

//output:
// sizes are added globally
// find(...) returns global labels


/*************************************
 * Derived Classes
 *************************************/
template<Geometry GEOMETRY>
class UnionFindGlobal : public UnionFindGlobal_CRTP<GEOMETRY, UnionFindGlobal<GEOMETRY>>{
public:
	static constexpr Geometry geometry = GEOMETRY;
	typedef UnionFindGlobal_CRTP<geometry, UnionFindGlobal<geometry>> super;
	typedef typename super::glob_size_t glob_size_t;
	typedef typename super::glob_label_t glob_label_t;
	typedef typename super::uf_t uf_t;

	friend UnionFindGlobal_CRTP<geometry, UnionFindGlobal<geometry>>;

	UnionFindGlobal(RegionGrid<geometry> && reg_grid, std::vector<uf_t> && u_finds):
			super(std::move(reg_grid), std::move(u_finds)){
		glob_sizes.push_back(0);
	};

	inline glob_label_t find(store_t label,size_t region_idx);

	glob_size_t get_cluster_size(store_t local_label, const size_t& region_idx);
	glob_size_t get_cluster_size(const glob_label_t glob_label);

	//temporary for benchmarking
	size_t size() {
		size_t size = 0;
		for(auto& uf : static_cast<super*>(this) -> union_finds){
			size += uf.csize.size() - 2;
		}
		return size + glob_sizes.size() - 1;
	}

private:
	inline void merge_both_local(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2);
	inline void merge_both_global(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2);
	inline void merge_global_and_local(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2);

	inline glob_label_t find_global_root(glob_label_t label){
		assert(label < glob_sizes.size());
		while(glob_sizes[label] < 0)
			label = -glob_sizes[label];
		return label;
	}

	std::vector<glob_size_t> glob_sizes; // initialized with one zero element, stores sizes with index = global Label without total_offset
};

template<Geometry geometry>
typename UnionFindGlobal<geometry>::glob_label_t UnionFindGlobal<geometry>::find(store_t label,size_t region_idx) {
	if(label == 0)
		return 0;

	auto& ufind = static_cast<super*>(this) -> union_finds[region_idx];
	label = ufind.find_by_label(label);

	if(ufind.csize[label].is_global()){
		return find_global_root(ufind.csize[label]) + static_cast<super*>(this) -> total_offset;
	}else{
		return label + static_cast<super*>(this) -> offsets[region_idx];
	}
}

template<Geometry geometry>
void UnionFindGlobal<geometry>::merge_global_and_local(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2){
	auto& union_find1 = static_cast<super*>(this) -> union_finds[region_idx1];
	auto& union_find2 = static_cast<super*>(this) -> union_finds[region_idx2];


	auto glob_label = find_global_root(union_find1.csize[label1]);

	glob_sizes[glob_label] += union_find2.csize[label2];

	union_find2.csize[label2] = glob_label;
	union_find2.csize[label2].make_global();

}

template<Geometry geometry>
void UnionFindGlobal<geometry>::merge_both_global(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2) {
	auto& union_find1 = static_cast<super*>(this) -> union_finds[region_idx1];
	auto& union_find2 = static_cast<super*>(this) -> union_finds[region_idx2];

	const auto glob_label1 = find_global_root(union_find1.csize[label1]);
	const auto glob_label2 = find_global_root(union_find2.csize[label2]);

	if(glob_label1 != glob_label2){
		 glob_sizes[glob_label1] += glob_sizes[glob_label2];
		 glob_sizes[glob_label2] = - glob_label1;
	}
}

template<Geometry geometry>
void UnionFindGlobal<geometry>::merge_both_local(const store_t& label1, const size_t& region_idx1,const store_t& label2, const size_t& region_idx2) {
	auto& union_find1 = static_cast<super*>(this) -> union_finds[region_idx1];
	auto& union_find2 = static_cast<super*>(this) -> union_finds[region_idx2];

	union_find1.csize[label1].make_global();
	union_find2.csize[label2].make_global();

	//get new global label
	glob_sizes.push_back(0);
	auto glob_label = glob_sizes.size() - 1;

	glob_sizes[glob_label] += union_find1.csize[label1];

	union_find1.csize[label1] = glob_label;

	glob_sizes[glob_label] += union_find2.csize[label2];

	union_find2.csize[label2] = glob_label;

	//assert(union_find1.csize[label1].is_global() && union_find2.csize[label2].is_global());
}

template<Geometry geometry>
typename UnionFindGlobal<geometry>::glob_size_t UnionFindGlobal<geometry>::get_cluster_size(store_t label, const size_t& region_idx) {
	if(label == 0)
		return 0;

	auto& ufind = static_cast<super*>(this) -> union_finds[region_idx];
	label = ufind.find_by_label(label);

	if(ufind.csize[label].is_global()){
		return glob_sizes[find_global_root(ufind.csize[label])];
	}else{
		return static_cast<super*>(this) -> union_finds[region_idx].csize[label];
	}
}

template<Geometry geometry>
typename UnionFindGlobal<geometry>::glob_size_t UnionFindGlobal<geometry>::get_cluster_size(glob_label_t glob_label) {
	if(glob_label <= static_cast<super*>(this) -> total_offset){ //local
		//find region idx
		size_t region_idx = 0;
		for(size_t i = 1; i < static_cast<super*>(this) -> offsets.size(); ++i){
			if(glob_label < static_cast<super*>(this) -> offsets[i])
				break;
			++region_idx;
		}
		auto offset = static_cast<super*>(this) -> offsets[region_idx];
		assert(glob_label >= offset);
		assert(glob_label - offset < static_cast<super*>(this) -> union_finds[region_idx].csize.size());
		return static_cast<super*>(this) -> union_finds[region_idx].csize[glob_label - offset];

	}else{ //global
		glob_label -= static_cast<super*>(this) -> total_offset;
		glob_label = find_global_root(glob_label);
		assert(glob_label < glob_sizes.size());
		return glob_sizes[glob_label];
	}

}


#endif //PROJECT_GLOBAL_UNION_FIND_HPP

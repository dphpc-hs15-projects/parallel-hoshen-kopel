#ifndef REGION_HPP
#define REGION_HPP

#include <cassert>
#include <array>
#include <iostream>
#include "types.hpp"
#include "datastructures/matrix.hpp"
#include <mutex>
#include <bitset>

class Region {
public:
	size_t mStart;
	size_t mEnd;
	size_t nStart;
	size_t nEnd;

	std::array<Region*, 4> neighbours;

	std::mutex mutex;

	Region() : Region(0, 0, 0, 0) {};

	Region(size_t mStart_, size_t mEnd_, size_t nStart_, size_t nEnd_)
	 : mStart(mStart_), mEnd(mEnd_), nStart(nStart_), nEnd(nEnd_),
	   neighbours({nullptr, nullptr, nullptr, nullptr}) {
	   	if (mStart_ != 0 && mEnd_ != 0 && nStart_ != 0 && nEnd_ != 0) {
		 	assert(mStart < mEnd);
		 	assert(nStart < nEnd);
	 	}
	 }

	Region(const Region& region) = delete; // copy ctor

	Region(Region&& region) : // move ctor
		mStart(region.mStart), mEnd(region.mEnd), nStart(region.nStart), nEnd(region.nEnd),
		neighbours(std::move(region.neighbours)), index_(region.index_) {

		Region* self(this);
		const auto update_topology = [&region, &self] (const NEIGHBOUR& forward, const NEIGHBOUR& backward) {
			if (region.neighbours[forward] != nullptr
			&& region.neighbours[forward]->neighbours[backward] == &region)
				region.neighbours[forward]->neighbours[backward] = self;
		};

		update_topology(NEIGHBOUR::TOP, NEIGHBOUR::BOTTOM);
		update_topology(NEIGHBOUR::BOTTOM, NEIGHBOUR::TOP);
		update_topology(NEIGHBOUR::LEFT, NEIGHBOUR::RIGHT);
		update_topology(NEIGHBOUR::RIGHT, NEIGHBOUR::LEFT);
	}

	/* temporarly deactivated due to mutex in region -> no move/copy ctor todo: reactivate
	// refine region along the dimensions that have a 1 in the dim_mask
	Matrix<Region> refine(const std::bitset<2> dim_mask) const {
		std::vector<size_t> ms;
		ms.push_back(mStart);
		if (dim_mask[0])
			ms.push_back(mStart+rows()/2); // use std::round
		ms.push_back(mEnd);

		std::vector<size_t> ns;
		ns.push_back(nStart);
		if (dim_mask[1])
			ns.push_back(nStart+cols()/2);
		ns.push_back(nEnd);

		Matrix<Region> grid(ms.size()-1, ns.size()-1);

		for (size_t i=0; i<ms.size()-1; ++i) {
			for (size_t j=0; j<ns.size()-1; ++j) {
				grid(i, j) = Region(ms[i], ms[i+1], ns[j], ns[j+1]);
			}
		}

		return grid;
	}*/

	size_t index() const {
		return index_;
	}

	~Region() = default; // default destructor

	Region& operator= ( Region&& ) = default; // move assignment op

	inline size_t rows() const { return mEnd - mStart; }

	inline size_t cols() const { return nEnd - nStart; }

private:
	template <Geometry> friend class RegionGrid;

	Region(size_t index, size_t mStart_, size_t mEnd_, size_t nStart_, size_t nEnd_) : Region(mStart_, mEnd_, nStart_, nEnd_) {
		index_ = index;
	}

	size_t index_; // used by global union find

};

inline std::ostream& operator<< (std::ostream& stream, const Region& r) {
    return stream << "[" << r.mStart << "," << r.mEnd << "[ x [" << r.nStart << "," << r.nEnd << "[";
}

#endif
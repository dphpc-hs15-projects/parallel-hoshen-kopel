#ifndef PROJECT_STATISTIC_HPP
#define PROJECT_STATISTIC_HPP

#include <string>
#include <map>
#include <list>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>

namespace acc = boost::accumulators;

/**
 * stores values identified by category and name
 */
class Statistic{
public:
	typedef std::string key_t;
	typedef long double value_t;
	typedef acc::accumulator_set<long double, acc::stats<acc::tag::sum, acc::tag::min, acc::tag::max, acc::tag::mean, acc::tag::variance(acc::lazy)> > list_t;
	typedef unsigned int count_t;
	typedef std::map<key_t, list_t > data_cont_t;
	typedef std::map<key_t, data_cont_t > category_cont_t;


	/*
	 * adds value identified with name and category.
	 * creates new category if non existent.
	 * if identifier already exits, value is added to stored value.
	 */
	static void add_to_category(const key_t category, const key_t name, const value_t value);

	/**
	 * print average values to console
	 */
	static void print_data();

	/*
	 * exports all averages in json format
	 */
	static void json_export(const key_t file_name);


private:
	static category_cont_t data;



};


#ifdef ACTIVATE_INTEL_PCM
#include "intel_counter/cpucounters.h"
#endif

/**
 * safes intel pcm data in statistic
 * does nothing if macro not defined
 */
class Intel_PCM{
public:
	/**
	 * use to safe state before starting measurement
	 */
	static void safe_pcm_state();
	/**
	 * adds pcm data to category
	 */
	static void add_pcm_to_category(const Statistic::key_t category);
private:
#ifdef ACTIVATE_INTEL_PCM
	static SystemCounterState state;
#endif

};


#endif //PROJECT_STATISTIC_HPP

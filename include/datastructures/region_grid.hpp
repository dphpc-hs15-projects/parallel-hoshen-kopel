#ifndef REGION_GRID_HPP
#define REGION_GRID_HPP

#include <cassert>
#include <utility>
#include <cmath>
#include "types.hpp"
#include "datastructures/region.hpp"

typedef Region region_t;
typedef Region* region_ptr;
typedef std::pair<region_ptr, region_ptr> border_t;


template <typename T> 
inline T getOptimalPartition(T n) {
	T k = std::sqrt(n);
	while (n % k != 0) --k;
	return k;
}


template <Geometry type>
struct RegionGrid {};

template <>
struct RegionGrid<Geometry::stripes> {
	std::vector<region_t> regions; // only read operations allowed!

	RegionGrid(RegionGrid&&) = default;
	RegionGrid(const RegionGrid&) = delete;

	RegionGrid(size_t m, size_t n, size_t lines) {
		assert(lines != 0);
		assert(m!=0);
		assert(n!=0);

		size_t height = m / lines;
		regions.reserve(lines);
		for (unsigned line = 0; line < lines-1; ++line){
			regions.push_back(Region(line, line * height, (line + 1) * height, 0, n));
		}
		regions.push_back(Region(lines-1, (lines-1)*height, lines*height + (m % lines), 0, n));

		// add topology information
		for (unsigned line = 0; line < lines; ++line){
			if (line!=0)
				regions[line].neighbours[NEIGHBOUR::TOP] = &regions[line-1];
			if (line!=lines-1)
				regions[line].neighbours[NEIGHBOUR::BOTTOM] = &regions[line+1];
		}
	}

	/*size_t index(const Region& region) {
		assert(regions.data() <= &region && &region < regions.data()+regions.size());
		return &region - regions.data();
	}*/

	std::vector<border_t> borders() {
		std::vector<border_t> borders;
		borders.reserve(regions.size()-1);
		for (unsigned i=0; i<regions.size()-1; ++i) {
			Region& r1 = regions[i];
			Region& r2 = regions[i+1];

			assert(r1.mEnd == r2.mStart);
			assert(r1.nStart == r2.nStart);
			assert(r1.nEnd == r2.nEnd);

			borders.emplace_back(&r1,&r2);
		}
		return borders;
	}
};

template <>
struct RegionGrid<Geometry::tiles> {
	size_t num_cols;
	size_t num_rows;

	std::vector<region_t> regions; // only read operations allowed!

	RegionGrid(size_t m, size_t n, size_t num_regions) {
		assert(num_regions != 0);

		num_cols = getOptimalPartition(num_regions);
		num_rows = num_regions / num_cols;

		size_t height = m / num_rows;
		size_t width = n / num_cols;
		size_t width_rem (0), height_rem (0);

		regions.reserve(num_cols*num_rows);
		/*
		 * regions are stored in a row major fashion e.g.
		 * 1 2
		 * 3 4
		 * 5 6
		 */
		for (unsigned row = 0; row < num_rows; ++row) {
			height_rem = (row == num_rows-1)? m % num_rows : 0;
			for (unsigned col = 0; col < num_cols; ++col) {
				width_rem = (col == num_cols-1)? n % num_cols : 0;
				regions.push_back(Region(row*num_cols+col, row*height,(row+1)*height + height_rem, col*width, (col+1)*width + width_rem));
			}
		}
	}

	std::vector<border_t> borders() {
		std::vector<border_t> borders;
		size_t num_borders = num_cols * (num_rows-1) + num_rows * (num_cols-1);
		borders.reserve(num_borders);
		for (unsigned i=0; i<num_cols*(num_rows-1); ++i) {
				Region& r1 = regions[i];
				Region& r2 = regions[i + num_cols];

				assert(r1.mEnd == r2.mStart);
				assert(r1.nStart == r2.nStart);
				assert(r1.nEnd == r2.nEnd);

				borders.emplace_back(&r1, &r2);
		}

		for (unsigned i=0; i<num_rows; ++i) {
			for (unsigned j=0; j<num_cols-1; ++j) {

				Region& r1 = regions[i*num_cols+j];
				Region& r2 = regions[i*num_cols+j+1];

				assert(r1.mStart == r2.mStart);
				assert(r1.mEnd == r2.mEnd);
				assert(r1.nEnd == r2.nStart);

				borders.emplace_back(&r1, &r2);
			}
		}
		return borders;
	}
};
#endif
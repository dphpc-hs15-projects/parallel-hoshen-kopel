#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <cassert>
#include <vector>

template <class T>
struct Matrix {
	const size_t m; // rows
	const size_t n; // cols

	std::vector<T> data;

	typedef typename std::vector<T>::reference reference;

	typedef typename std::vector<T>::const_reference const_reference;

	typedef typename std::vector<T>::value_type value_type;

	typedef typename std::vector<T>::pointer pointer;

	typedef typename std::vector<T>::const_pointer const_pointer;

	typedef typename std::vector<T>::const_iterator const_iterator;

	typedef typename std::vector<T>::iterator iterator;

	Matrix(const size_t m_, const size_t n_) : m(m_), n(n_), data(m_*n_) {}

	inline reference operator()(const size_t& i, const size_t& j) {
		assert(i >= 0);
		assert(j >= 0);
		assert(i < m);
		assert(j < n);

		return data[index(i, j)];
	};

	inline const_reference operator()(const size_t& i, const size_t& j) const {
		assert(i >= 0);
		assert(j >= 0);
		assert(i < m);
		assert(j < n);

		return data[index(i, j)];
	};

	inline const_iterator cbegin() const {
		return data.cbegin();
	}

	inline const_iterator cend() const {
		return data.cend();
	}

	inline iterator begin() {
		return data.begin();
	}

	inline iterator end() {
		return data.end();
	}

	inline size_t index(const size_t i, const size_t j) const {
		return i*n+j;
	}

	inline const size_t rows() { return m; }

	inline const size_t cols() { return n; }

	template <typename T1, typename T2, typename Enable = void>
	struct VectorCompare { bool equal(const std::vector<T1>&, const std::vector<T2>&) { return false; }; };

	template <typename T1, typename T2>
	struct VectorCompare<T1, T2, typename std::enable_if<std::is_same<T1, T2>::value>::type> { 
		bool equal(const std::vector<T1>& v1, const std::vector<T2>& v2) { return v1 == v2; };
	};

	template <typename U>
	bool operator==(const Matrix<U> matrix) const {
		return VectorCompare<T, U>().equal(data, matrix.data);
	}

	template <typename U>
	bool operator!=(const Matrix<U> matrix) const { return !(matrix == *this); }
};
#endif
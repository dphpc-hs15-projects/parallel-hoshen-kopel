#ifndef LOCAL_LOBAL_HPP
#define LOCAL_LOBAL_HPP
#include <climits>
#include "../types.hpp"
#include "cassert"

template <class STORE_T>
struct CSizeNode {
#ifndef NDEBUG
	enum TYPE : bool {
		SIZE,
		REFERENCE
	};
#endif

	STORE_T val;

#ifdef NDEBUG
	CSizeNode(STORE_T val_) : val(2*val_) {}
	CSizeNode(): val(0) {}
#else
	TYPE type;

	CSizeNode(STORE_T val_) : val(2*val_), type(val_ < 0 ? REFERENCE : SIZE) {}
	CSizeNode(): val(0), type(SIZE) {}
#endif

	CSizeNode(CSizeNode&) = delete;
	CSizeNode(CSizeNode&&) = default;

	/*CSizeNode(CSizeNode&) {
		assert(type == TYPE::SIZE);
	};*/

	bool is_global() const {
		assert(type == TYPE::SIZE);
		return val & 0x1;
	}

	void make_global() {
		assert(type == TYPE::SIZE);
		val = val | 0x1;
	}

	void make_local() {
		assert(type == TYPE::SIZE);
		val = val & ~0x1;
	}

	operator STORE_T() const {
		return val/2;
	}

	CSizeNode& operator++() { val += 2; return *this; }

	CSizeNode& operator+=(const STORE_T& operand) { assert(type == TYPE::SIZE); val += operand*2; return *this; }

	CSizeNode& operator-=(const STORE_T& operand) { assert(type == TYPE::SIZE); val -= operand*2; return *this; }

	CSizeNode& operator*=(const STORE_T& operand) { assert(type == TYPE::SIZE); val *= operand*2; return *this; }

	CSizeNode& operator/=(const STORE_T& operand) { assert(type == TYPE::SIZE); val /= operand*2; return *this; }

	CSizeNode& operator=(const STORE_T& operand) { val = operand*2 | (val & 0x1); return *this; }

	CSizeNode& operator=(const CSizeNode& operand) { val = operand.val; return *this; } // todo assert
};

#endif
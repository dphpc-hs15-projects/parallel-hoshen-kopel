#ifndef UNION_FIND_HPP
#define UNION_FIND_HPP

#include <vector>
#include <limits>
#include "types.hpp"
#include "datastructures/matrix.hpp"
#include "datastructures/csize_node.hpp"

template <typename STORE_T=store_t, typename LABEL_T=STORE_T, template <typename, typename> class CSIZE_T=std::vector>
struct UnionFind {
	typedef LABEL_T label_t; // type of the elements in csize (must be convertable to store_t)
	typedef STORE_T store_t; // type of the elements in field
	typedef CSIZE_T<label_t, std::allocator<label_t>> csize_t;
	typedef Matrix<store_t> field_t;

	typedef label_t value_type; // for compatibility

	size_t max_size;
	field_t& field;
	csize_t csize;

	UnionFind(size_t max_size_, field_t& field_)
	 : field(field_), max_size(max_size_) {
	 	csize.reserve(max_size);
	 	csize.resize(2, 0);
		assert(max_size < std::numeric_limits<store_t>::max());
	}

	UnionFind(field_t& field_, csize_t&& csize_) : max_size(csize_.capacity()), field(field_), csize(std::move(csize_)) {
		assert(max_size < std::numeric_limits<store_t>::max());
	}

	UnionFind(UnionFind&) = delete;
	UnionFind(UnionFind&&) = default;

	unsigned redirections = 0;

	// TODO: make thread_safe via template parameter that specifies if the path should be compressed
	inline store_t find(const size_t& i, const size_t& j);

	inline store_t find_by_label(store_t& label);

	inline store_t find_by_label(const store_t& label);

	inline void explore(const size_t& i, const size_t& j);

	/**
	 * explore (i1, j1) and add it to the set of (i2, j2)
	 */
	inline void merge_explore(const size_t& i1, const size_t& j1, const size_t& i2, const size_t& j2);

	inline void merge_explore_by_label(const size_t& i, const size_t& j, const store_t& label);

	/**
	 * merge (i1, j1) and (i2, j2)
	 */
	inline store_t merge(const size_t& i1, const size_t& j1, const size_t& i2, const size_t& j2);

	inline store_t merge_by_label(store_t label1, store_t label2);

	inline size_t size() const { return csize.size(); }
};

template <typename STORE_T, typename LABEL_T, template <typename, typename> class CSIZE_T>
STORE_T UnionFind<STORE_T, LABEL_T, CSIZE_T>::find_by_label(store_t& label) {
	//store_t org_label = label;

	while(csize[label] < 0) {
		//if (csize[-csize[label]] < 0)
		//	csize[label] = csize[-csize[label]];
		label = - csize[label];
		redirections++;
	}

	//while (csize[org_label] < 0 && csize[org_label] != -label) {
	//	auto tmp = org_label;
	//	csize[org_label] = -label;
	//	org_label = -csize[tmp];
	//}

	return label;
}

template <typename STORE_T, typename LABEL_T, template <typename, typename> class CSIZE_T>
STORE_T UnionFind<STORE_T, LABEL_T, CSIZE_T>::find_by_label(const store_t& label) {
	return find_by_label(label);
}

template <typename STORE_T, typename LABEL_T, template <typename, typename> class CSIZE_T>
STORE_T UnionFind<STORE_T, LABEL_T, CSIZE_T>::find(const size_t& i, const size_t& j) {
	return find_by_label(field(i, j));
}

/*store_t UnionFind<CSIZE_T>::find(const store_t& element) {
	return find_by_label(element);
}*/

template <typename STORE_T, typename LABEL_T, template <typename, typename> class CSIZE_T>
void UnionFind<STORE_T, LABEL_T, CSIZE_T>::explore(const size_t& i, const size_t& j) {
	assert(size() < max_size);
	field(i, j) = size();
	csize.push_back(1);
}

/**
 * explore (i1, j1) and add it to the set of (i2, j2)
 */
template <typename STORE_T, typename LABEL_T, template <typename, typename> class CSIZE_T>
void UnionFind<STORE_T, LABEL_T, CSIZE_T>::merge_explore(const size_t& i1, const size_t& j1, const size_t& i2, const size_t& j2) {
	merge_explore_by_label(i1, j1, find(i2, j2));
}

template <typename STORE_T, typename LABEL_T, template <typename, typename> class CSIZE_T>
void UnionFind<STORE_T, LABEL_T, CSIZE_T>::merge_explore_by_label(const size_t& i, const size_t& j, const store_t& label) {
	field(i, j) = label;
	csize[label] += 1;
}

/**
 * merge (i1, j1) and (i2, j2)
 */
template <typename STORE_T, typename LABEL_T, template <typename, typename> class CSIZE_T>
STORE_T UnionFind<STORE_T, LABEL_T, CSIZE_T>::merge(const size_t& i1, const size_t& j1, const size_t& i2, const size_t& j2) {
	return field(i1, j1) = field(i2, j2) = merge_by_label(find(i1, j1), find(i2, j2));
}

template <typename STORE_T, typename LABEL_T, template <typename, typename> class CSIZE_T>
STORE_T UnionFind<STORE_T, LABEL_T, CSIZE_T>::merge_by_label(store_t label1, store_t label2) {
	assert(label1!=label2);

	if (csize[label2] > csize[label1]) // union by size
		std::swap(label1, label2);

	csize[label1] += csize[label2];
	csize[label2] = -label1;

	return label1;
}

#endif
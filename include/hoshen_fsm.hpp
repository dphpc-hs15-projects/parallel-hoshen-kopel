#ifndef HOSHEN_FSM_HPP
#define HOSHEN_FSM_HPP

#include "datastructures/union_find.hpp"
#include "datastructures/region.hpp"

template<typename T1, typename T2, typename LABEL_T = typename T2::value_type>
UnionFind<typename T2::value_type, LABEL_T> hoshen_fsm(const T1& field, T2& output) {
	using store_ptr_t = typename T2::pointer;

	const Region region(0, field.m, 0, field.n);
	auto result = hoshen_fsm<T1, T2, LABEL_T>(field, output, region);
	auto& csize = result.csize;

	store_t tmp1, tmp2;

	//unify cluster labels
	store_ptr_t current_ptr=&output(0, 0);

	for(size_t i=region.mStart; i<region.mEnd; ++i) {
		current_ptr=&output(i, region.nStart);
		for(size_t j=region.nStart; j<region.nEnd; ++j){
			assert(current_ptr-&output(0, 0) == output.index(i, j));
			if ((*current_ptr > 0) && (csize[*current_ptr] < 0)) {
				tmp1 = csize[*current_ptr];
				while (tmp1 < 0) { // chase negative
					tmp2 = -tmp1;
					tmp1 = csize[tmp2];
				}
				while (*current_ptr != tmp2) { // compress search path
					tmp1 = -csize[*current_ptr];
					csize[*current_ptr] = -(tmp2);
					*current_ptr = tmp1;
				}
			}
			current_ptr++;
		}
	}

	return result;
}

template<typename T1, typename T2, typename LABEL_T = typename T2::value_type>
UnionFind<typename T2::value_type, LABEL_T> hoshen_fsm(const T1& field, T2& output, const Region& region) {
	// types
	using label_t = LABEL_T;
	using store_t = typename T2::value_type;
	using uf_t = UnionFind<store_t, label_t>;
	using field_value_t = typename T1::value_type;
	using field_value_ptr_t = typename T1::const_iterator;
	using store_ptr_t = typename T2::pointer;

	// validate input args
	assert(field.m != 0);
	assert(field.n != 0);
	assert(field.m == output.m);
	assert(field.n == output.n);
	assert(region.nEnd <= field.n);
	assert(region.mEnd <= field.m);

	const size_t m = region.rows();
	const size_t n = region.cols();

	field_value_ptr_t current_field_ptr=field.cbegin()+field.index(region.mStart, region.nStart);
	store_ptr_t current_ptr=&output(region.mStart, region.nStart);
	store_ptr_t north_ptr=nullptr;
	store_t north; // temporary variable to store the north value
	store_t tmp1, tmp2;
	store_t previous; // left value

	typename uf_t::csize_t csize(2, 0);
	csize.reserve(n*m/2+3);

	// s0 - Not currently in a cluster
	// s1 - Currently in a cluster on this line
	// s2 - Currently in a cluster on previous line
	uint8_t state = 0;

	constexpr bool not_equal = !std::is_same<T1, T2>::value;

	for(size_t i=region.mStart; i<region.mEnd; ++i){
		for(size_t j=region.nStart; j<region.nEnd; ++j){
			assert(current_ptr-&output(0, 0) == output.index(i, j));
			assert(current_field_ptr-field.cbegin() == field.index(i, j));
			assert(i==region.mStart || north_ptr-&output(0, 0) == output.index(i-1, j));

			if (not_equal) *current_ptr=*current_field_ptr;

			if (*current_field_ptr) {
				north = (i==region.mStart) ? 0 : *north_ptr;

				switch (state) {
					case 0: // not currently on a cluster
						if (north == 0) {
							*current_ptr = csize.size(); // explore
							csize.push_back(1);
							state = 1; // switch to s1
						} else { // merge with cluster above
							if (csize[north] < 0) { // do we have to compress the path of the top cluster
								// find root label of the north_ptr cluster
								tmp1 = csize[north];
								while (tmp1 < 0) { // chase negative
									tmp2 = -tmp1;
									tmp1 = csize[-tmp1];
								}
								*current_ptr = tmp2; // write link to the current_ptr root label

								// compress north_ptr path
								while (north != *current_ptr) {
									tmp1 = -csize[north];
									csize[north] = -(*current_ptr);
									north = tmp1;
								}
								*north_ptr = north;
							} else {
								// *N > 0
								*current_ptr = north;
							}
							++csize[*current_ptr];
							state = 2; // switch to s2
						}
						previous = *current_ptr;
						break;
					case 1:
						if (north == 0)
							state = 1;

						*current_ptr = previous;
						++csize[*current_ptr];

						if (north == 0)
							break;
						else
							state = 2; // switch to state 2 without moving to the next output
					case 2:
						if (north != 0) {
							state = 2;
							*current_ptr = previous;
							++csize[*current_ptr];
							// merge north into current_ptr operation
							if (north != *current_ptr && -csize[north] != *current_ptr) {
								if (csize[north] >= 0) {
									csize[*current_ptr] += csize[north];
									csize[north] = -(*current_ptr);
								} else {
									tmp1 = -csize[north];
									csize[north] = -(*current_ptr);
									while (tmp1 > 0) {
										// chase negative
										tmp2 = tmp1;
										tmp1 = -csize[tmp1];
										csize[tmp2] = -(*current_ptr);
									}
									if (tmp2 == *current_ptr) // no merge necessary
										csize[tmp2] = -tmp1; // restore csize
									else
										csize[*current_ptr] -= tmp1;
								}
							}
						} else {
							*current_ptr = previous;
							state = 1;
							++csize[*current_ptr];
						}
						break;
				}
			} else {
				state = 0;
			}
			current_field_ptr++;
			current_ptr++;
			north_ptr++;
		}
		current_field_ptr=field.cbegin()+field.index(i+1, region.nStart);
		current_ptr=&*output.begin()+output.index(i+1, region.nStart);
		north_ptr=&output(i, region.nStart);
		state = 0; // end of row return to state 0
	}

	return uf_t(output, std::move(csize));
}
#endif
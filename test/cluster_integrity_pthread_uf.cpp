#include <vector>
#include "types.hpp"
#include "catch.hpp"
#include "utilities.hpp"
#include "datastructures/matrix.hpp"
#include "hoshen_uf_pthreads.hpp"

#include <map>
#include <set>

TEST_CASE("test_cluster_integrity_pthread_uf", "[cluster_integrity_pthread_uf]") {
	const size_t m = 20u;
	const size_t n = 20u;
	Matrix<bool> field(m,n);
	Matrix<store_t> outputField(m,n);
	create_perc(field, 0.6);
	auto uf_global = hoshen_kopel_uf_pthreads<Geometry::stripes, Merge_Type::parallel>(field, outputField, 4);
	store_t top,left,bottom,right,current;
	
	// checks that the neighbouring positions in the field belong to the same cluster 
	SECTION("test cluster input/output coherence") {
		for(size_t i = 0; i < m; ++i) {
			for (size_t j = 0; j < n; ++j) {
				if(field(i,j))
					REQUIRE(outputField(i,j) != 0);
				else
					REQUIRE(outputField(i,j) == 0);
			}
		}
	}

	SECTION("test cluster integrity") {
		for(size_t i = 0; i < m; ++i){
			for(size_t j = 0; j < n; ++j){
				current = outputField(i,j);

				if (current != 0) {
					top = i < 1 ? 0 : outputField(i-1, j);
					left = j < 1 ? 0 : outputField(i, j-1);
					bottom = i > m-2 ? 0 : outputField(i+1, j);
					right = j > n-2 ? 0 : outputField(i, j+1);


					REQUIRE((top == 0 || current == top));
					REQUIRE((left == 0 || current == left));
					REQUIRE((right == 0 || current == right));
					REQUIRE((bottom == 0 || current == bottom));
				}
			}
		}
	}

	//checks that the masses of the individual clusters are correctly saved in the list
	SECTION("test cluster masses") {
		size_t size = uf_global.size();
		std::map<store_t, size_t> checklist;

		for(size_t i = 0; i < m; ++i){
			for(size_t j = 0; j < n; ++j){
				auto label = outputField(i, j);
				if (outputField(i, j) != 0) {
					if(checklist.count(label) == 0)
						checklist[label] = 0;
					checklist[label] += 1;
				}
			}
		}

		for(auto& pair : checklist){
			INFO( "cluster id: " << pair.first );
			REQUIRE(pair.second == uf_global.get_cluster_size(pair.first));
		}

	}

	// checks that the total mass of all clusters combined is equal to the mass of all clusters combined
	// saved in the list
	SECTION("test total mass of cluster") {
		size_t mass_list(0);
		size_t mass_cluster(0);

		for(size_t i = 0; i < m; ++i){
			for(size_t j = 0; j < n; ++j){
				if (outputField(i, j) != 0)
					++mass_cluster;
			}
		}

		//find all labels in field
		std::set<store_t> labels;
		for(size_t i = 0; i < m; ++i){
			for(size_t j = 0; j < n; ++j){
				auto label = outputField(i, j);
				if (label != 0) {
					labels.insert(label);
				}

			}
		}

		//add up sizes of all clusters in global union find
		size_t mass_cluster_uf_global = 0;
		for(auto& val : labels)
			mass_cluster_uf_global += uf_global.get_cluster_size(val);

		REQUIRE(mass_cluster == mass_cluster_uf_global);
	}

}
#include <vector>
#include "types.hpp"
#include "catch.hpp"
#include "utilities.hpp"
#include "datastructures/matrix.hpp"
#include "hoshen_naive.hpp"
#include "hoshen_uf.hpp"
#include "hoshen_kopel_fsm.hpp"
#include "datastructures/region.hpp"
#include "datastructures/union_find.hpp"

TEST_CASE("test_cluster_integrity_uf", "[cluster_integrity_uf]") {
	typedef UnionFind<> uf_t;
	typedef Matrix<bool> field_t;
	typedef Matrix<store_t> output_field_t;

	std::array<uf_t(*)(const field_t&, output_field_t&), 3> implementations{
		hoshen_naive<field_t, output_field_t>,
		hoshen_uf<field_t, output_field_t>,
		hoshen_fsm<field_t, output_field_t>
	};

	const size_t m = 100u;
	const size_t n = 150u;
	field_t field(m,n);
	create_perc(field, 0.6);

	for (auto hoshen_kopel : implementations) {
		output_field_t outputField(m,n);
		uf_t union_find = hoshen_kopel(field,outputField);
		store_t top,left,bottom,right,current;

		// check that every occupied field is labeled
		SECTION("test cluster input/output coherence") {
			for(size_t i = 0; i < m; ++i) {
				for (size_t j = 0; j < n; ++j) {
					INFO("i: " + std::to_string(i) + ", j:" + std::to_string(j));
					if(field(i,j))
						REQUIRE(outputField(i,j) != 0);
					else
						REQUIRE(outputField(i,j) == 0);
				}
			}
		}

		// check that neighboring positions in the field belong to the same cluster
		SECTION("test cluster integrity") {
			for(size_t i = 0; i < m; ++i){
				for(size_t j = 0; j < n; ++j){
					current = union_find.find(i,j);

					if (current != 0) {
						top = i < 1 ? 0 : union_find.find(i-1, j);
						left = j < 1 ? 0 : union_find.find(i, j-1);
						bottom = i > m-2 ? 0 : union_find.find(i+1, j);
						right = j > n-2 ? 0 : union_find.find(i, j+1);

						REQUIRE((top == 0 || current == top));
						REQUIRE((left == 0 || current == left));
						REQUIRE((right == 0 || current == right));
						REQUIRE((bottom == 0 || current == bottom));
					}
				}
			}
		}

		// checks that the masses of the individual clusters are correctly saved in the list
		SECTION("test cluster masses") {

			size_t size = union_find.csize.size();
			csize_t checklist(size,0);

			for(size_t i = 0; i < m; ++i){
				for(size_t j = 0; j < n; ++j){
					auto label = outputField(i, j);
					if (label != 0) {
						checklist[union_find.find_by_label(label)] += 1;
					}
				}
			}

			for (int i = 0; i < size; ++i) {
				if (checklist[i] > 0) {
					INFO( "cluster id: " << i );
					REQUIRE(checklist[i] == union_find.csize[i]);
				}
			}
		}

		// checks that the total mass of all clusters is equal total mass in csize
		SECTION("test total mass of cluster") {
			
			size_t mass_list(0);
			size_t mass_cluster(0);

			for(size_t i = 0; i < m; ++i){
				for(size_t j = 0; j < n; ++j){
					if (outputField(i, j) != 0)
						++mass_cluster;
				}
			}

			size_t size = union_find.csize.size();
			for (size_t i = 0; i < size; ++i) {
				store_t element = union_find.csize[i];
				if (element > 0)
					mass_list += element;
			}

			REQUIRE(mass_cluster == mass_list);
		}
	}
}
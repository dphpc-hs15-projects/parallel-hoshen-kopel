#include <vector>
#include "types.hpp"
#include "catch.hpp"
#include "hoshen_naive.hpp"
#include "hoshen_uf.hpp"
#include "hoshen_kopel_fsm.hpp"
#include "datastructures/matrix.hpp"
#include "utilities.hpp"

TEST_CASE("test csize cycles", "[csize]") {
	typedef UnionFind<> uf_t;
	typedef Matrix<bool> field_t;
	typedef Matrix<store_t> output_field_t;

	std::array<uf_t(*)(const field_t&, output_field_t&), 3> implementations{
		hoshen_naive<field_t, output_field_t>,
		hoshen_uf<field_t, output_field_t>,
		hoshen_fsm<field_t, output_field_t>
	};

	const size_t m = 300u;
	const size_t n = 100u;
	Matrix<bool> field(m,n);
	create_perc(field,0.6);
	

	// verify that no reference to a cluster in csize points to itself
 	for (auto hoshen_kopel : implementations) {
 		Matrix<store_t> outputField(m,n);
 		auto result = hoshen_naive(field,outputField);
		auto& csize = result.csize;

		SECTION("test csize cycles") {
			csize_t checklist;
			size_t size = csize.size();
			size_t pos = 0;
			for (size_t i = 0; i < size; ++i) {
				pos = i;
				while (csize[pos] < 0) {
					auto p = std::find(checklist.begin(),checklist.end(),csize[pos]);
					INFO("Cycle starts at index: " << i);
					REQUIRE(p == checklist.end());
					checklist.push_back(csize[pos]);		
					pos = -csize[pos];
				}
				checklist.clear();
			}
		}
	}
}
#include <datastructures/region_grid.hpp>
#include "types.hpp"
#include "catch.hpp"
#include "datastructures/global_union_find.hpp"
#include "datastructures/union_find.hpp"
#include "iostream"

TEST_CASE("Global Union Find", "[GLobalUnionFind]") {
	const Geometry geometry = Geometry::stripes;
	typedef UnionFindGlobal<geometry> uf_global_t;
	typedef uf_global_t::uf_t uf_t;
	
	SECTION("Global Union Find") {
		size_t m = 10;
		size_t n = 10;
		size_t nThreads = 2;
		Matrix<store_t> field(n,m);
		
		field(4,0) = 1;
		field(4,1) = 1;

		field(4,3) = 1;
		field(4,4) = 1;
		field(4,5) = 1;

		field(4,7) = 1;

		field(5,1) = 1;
		field(5,2) = 1;
		field(5,3) = 1;

		field(5,5) = 1;
		field(5,6) = 1;
		field(5,7) = 1;

		RegionGrid<geometry> region_grid(m, n, nThreads);

		std::vector<uf_t> ufinds;
		ufinds.reserve(region_grid.regions.size());
		ufinds.emplace_back(m*n+3,field);
		ufinds.emplace_back(m*n+3,field);

		UnionFindGlobal<geometry> uf_global(std::move(region_grid), std::move(ufinds));

		// merge locally
		uf_global.union_finds[0].explore(4,0); 				//label 2
		uf_global.union_finds[0].merge_explore(4,1,4,0); 	//label 2
		uf_global.union_finds[0].explore(4,3); 				//label 3
		uf_global.union_finds[0].merge_explore(4,4,4,3); 	//label 3
		uf_global.union_finds[0].merge_explore(4,5,4,3); 	//label 3
		uf_global.union_finds[0].explore(4,7); 				//label 4

		uf_global.union_finds[1].explore(5,1); 				//label 2
		uf_global.union_finds[1].merge_explore(5,2,5,1); 	//label 2
		uf_global.union_finds[1].merge_explore(5,3,5,1); 	//label 2
		uf_global.union_finds[1].explore(5,5);				//label 3
		uf_global.union_finds[1].merge_explore(5,6,5,5); 	//label 3
		uf_global.union_finds[1].merge_explore(5,7,5,5); 	//label 3

		//check zero label
		CHECK(uf_global.find(0,0)==0);

		//both labels local
		CHECK(uf_global.find(2,0) == 2);
		CHECK(uf_global.find(2,1) == 4);
		uf_global.merge(2,0,2,1);
		CHECK(uf_global.find(2,0) == uf_global.find(2,1));

		CHECK(uf_global.find(4,0) == 4);
		CHECK(uf_global.find(3,1) == 5);
		uf_global.merge(4,0,3,1);
		CHECK(uf_global.find(4,0) == uf_global.find(3,1));
		CHECK_FALSE(uf_global.find(4,0) == uf_global.find(2,1));

		//one global, one local
		CHECK(uf_global.find(3,0) == 3);
		uf_global.merge(3,0,2,1);
		CHECK(uf_global.find(3,0) == uf_global.find(2,1));

		//both the same global label
		CHECK(uf_global.find(3,0) == uf_global.find(2,1));
		uf_global.merge(3,0,2,1);
		CHECK(uf_global.find(3,0) == uf_global.find(2,1));

		//merge two global labels
		CHECK_FALSE(uf_global.find(3,0) == uf_global.find(3,1));
		uf_global.merge(3,0,3,1);
		CHECK(uf_global.find(4,0) == uf_global.find(3,1));
		CHECK(uf_global.find(4,0) == uf_global.find(3,0));
		CHECK(uf_global.find(4,0) == uf_global.find(2,1));
		CHECK(uf_global.find(4,0) == uf_global.find(2,0));
	}
}
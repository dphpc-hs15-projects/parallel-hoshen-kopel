#include "catch.hpp"
#include "types.hpp"
#include "datastructures/region_grid.hpp"
#include <iostream>


TEST_CASE("test_regions_integrity", "[regions_integrity]") {
	size_t m = 100u;
	size_t n = 100u;
	size_t nThreads = 8;

	SECTION("Regions with stripes") {

		const Geometry geometry = Geometry::stripes;
		RegionGrid<geometry> region_grid(m, n, nThreads);
		SECTION("test number of regions") {
			CHECK(nThreads == region_grid.regions.size());
		}

		SECTION("test number of borders") {
			CHECK (region_grid.borders().size() == 7);
		}

		SECTION("calculate area covered by regions") {
			unsigned long area (0);
			for (auto& element : region_grid.regions) {
				area += (element.nEnd - element.nStart) * (element.mEnd - element.mStart);
			}
			CHECK(area == m*n);
		}

		SECTION("Region grid topology") {
			for (Region& region : region_grid.regions) {
				bool top_check = region.neighbours[NEIGHBOUR::TOP] ? &region == region.neighbours[NEIGHBOUR::TOP]->neighbours[NEIGHBOUR::BOTTOM] : true;
				bool bottom_check = region.neighbours[NEIGHBOUR::BOTTOM] ? &region == region.neighbours[NEIGHBOUR::BOTTOM]->neighbours[NEIGHBOUR::TOP] : true;
				CHECK(top_check);
				CHECK(bottom_check);
			}
		}

	}

	SECTION("Regions with tiles") {
		const Geometry geometry = Geometry::tiles;
		RegionGrid<geometry> region_grid(m, n, nThreads);
		auto borders = region_grid.borders();
		
		SECTION("test number of regions") {
			CHECK(nThreads == region_grid.regions.size());
		}
		SECTION("test number of borders") {
			CHECK (region_grid.borders().size() == 10);
		}

		SECTION("calculate area covered by regions") {
			unsigned long area (0);
			for (auto& element : region_grid.regions) {
				area += (element.nEnd - element.nStart) * (element.mEnd - element.mStart);
			}
			CHECK(area == m*n);
		}

		SECTION("Region grid topology") {
			for (Region& region : region_grid.regions) {
				bool left_check = region.neighbours[NEIGHBOUR::LEFT] ? &region == region.neighbours[NEIGHBOUR::LEFT]->neighbours[NEIGHBOUR::RIGHT] : true;
				bool right_check = region.neighbours[NEIGHBOUR::RIGHT] ? &region == region.neighbours[NEIGHBOUR::RIGHT]->neighbours[NEIGHBOUR::LEFT] : true;
				bool top_check = region.neighbours[NEIGHBOUR::TOP] ? &region == region.neighbours[NEIGHBOUR::TOP]->neighbours[NEIGHBOUR::BOTTOM] : true;
				bool bottom_check = region.neighbours[NEIGHBOUR::BOTTOM] ? &region == region.neighbours[NEIGHBOUR::BOTTOM]->neighbours[NEIGHBOUR::TOP] : true;

				CHECK(left_check);
				CHECK(right_check);
				CHECK(top_check);
				CHECK(bottom_check);
			}
		}
	}

	SECTION("Region move ctor") {
		// initialize 2 regions
		Region r1(0, 10, 0, 10);
		Region r2(0, 10, 0, 10);
		// connect them | r1 | r2 |
		r1.neighbours[NEIGHBOUR::RIGHT] = &r2;
		r2.neighbours[NEIGHBOUR::LEFT] = &r1;

		// move them into a pair
		Region r1_moved(std::move(r1));
		Region r2_moved(std::move(r2));

		// check that the topology is still correct
		CHECK(r1_moved.neighbours[NEIGHBOUR::RIGHT] == &r2_moved);
		CHECK(r2_moved.neighbours[NEIGHBOUR::LEFT] == &r1_moved);
	}

	SECTION("Region splitting") {
		Region region(0, 199, 0, 20); // todo: do not start at 0
		//region.refine(-1); // split along both dimensions

		/*temporarly deactivated due to mutex in region -> no move/copy ctor todo: reactivate
		// split along first dimension
		auto regions_split_1 = region.refine(1); // split along first dimension
		CHECK(regions_split_1.rows()==2);
		CHECK(regions_split_1.cols()==1);
		CHECK((regions_split_1(0, 0).rows() + regions_split_1(1, 0).rows()) == region.rows());
		CHECK(regions_split_1(0, 0).cols() == region.cols());
		CHECK(regions_split_1(1, 0).cols() == region.cols());

		// split along second dimension
		auto regions_split_2 = region.refine(2); // split along second dimension
		CHECK(regions_split_2.rows()==1);
		CHECK(regions_split_2.cols()==2);
		CHECK((regions_split_2(0, 0).cols() + regions_split_2(0, 1).cols()) == region.cols());
		CHECK(regions_split_2(0, 0).rows() == region.rows());
		CHECK(regions_split_2(0, 1).rows() == region.rows());*/
	}

}



#include "types.hpp"
#include "catch.hpp"
#include "datastructures/csize_node.hpp"

TEST_CASE("LocalLabels test", "[LocalLabels]") {
	CSizeNode<store_t> label(5);

	CHECK_FALSE(label.is_global());
	label.make_global();
	CHECK(label.is_global());

	label.make_local();
	CHECK_FALSE(label.is_global());

	CHECK(label == 5);
	CHECK((label + 1) == 6);
	CHECK((label += 1) == 6);
	CHECK(label > 0);

	const CSizeNode<store_t>& const_label(label);
	CHECK(const_label == 6);
	CHECK(const_label > 0);
	
	{
		CSizeNode<store_t> label(5);
		label.make_global();
		CSizeNode<store_t> label_copy = std::move(label);
		CHECK(label.is_global());
		label = 0;
		CHECK(label.is_global());
	}
}